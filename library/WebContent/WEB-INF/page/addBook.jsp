<%@page import="java.util.List"%>
<%@page import="entity.Book_klass"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./static/css/menu.css" type="text/css" rel="stylesheet">
<title>新增图书</title>
<script src="./static/js/jquery.js"></script>
<style type="text/css">
body {
	background-color: #F5F5F5
}

select {
	width: 100px;
	margin: 8px 3px;
	height: 30px;
	font-size: 15px;
	background-color: #F5FFFA
}

input {
	margin: 8px 3px;
	height: 25px;
	font-size: 15px;
	background-color: #F5FFFA;
}
#submitButton {
	width: 250px;
	font-size: 15px;
	background-color: #2E8B57;
	color: white;
	height: 35px;
}

#submitButton:hover {
	background-color: #008B8B;
}
#result{
		border: 1px solid #CCC;
		border-radius: 2%;
		width:400px;
		margin: 10px auto; 
}
</style>
</head>
<body>
	<h3>图书管理系统 - 新增图书</h3>
	<hr>
	<%@include file="./common/menu.jsp"%>
	<div class="buttomDiv">
	<form  enctype="multipart/form-data" id="uploadForm"  name="uploadForm">
		 <div id="ajaxFileinput"><input type="file" name="file" ><br></div>
		书名 :<input type="text" id="bookName" name="bookName"> <br>
		 所属类 : <select id="klassName" name="klassName">
			<%
				List<Book_klass> Book_klassList = (List<Book_klass>) request.getAttribute("book_klassList");
				for (Book_klass book_klass : Book_klassList) {
			%>
			<option value="<%=book_klass.getName()%>"><%=book_klass.getName()%></option>
			<%
				}
			%>
		</select><br> 
		<input type="button" value="新增图书入库" id="submitButton">
		</form>
		<br><br>
		<div id="result" >
		
		</div>
	</div>
</body>
<script type="text/javascript">
	$("#submitButton").click(function() {
		var bookName = $("#bookName").val();
		var klassName = $("#klassName").val();
		
		 var bookName2 = document.uploadForm.bookName.value;
		 console.log(bookName2);
		if (bookName == "") {
			alert('书名不能为空');
			return false;
		}
		var r=confirm("您确认后，我们将随机给您在"+klassName+"类随机分配一个图书位。前提要保证所在类图书位有空余。")
		if(r){
			//样式变化
			document.getElementById("submitButton").value = "请稍后,正在加载入库";
			document.getElementById("submitButton").disabled=true;
			var formData = new FormData($('#uploadForm')[0]);   
			 $.ajax({
	                url:"/library/BookServlet?method=ajax_addBook",
	                type:"post",
	                data:formData,
	                processData:false,
	                contentType:false,
	                success:function(data){     
	                    console.log(data);
	                	var obj = eval('(' + data+ ')');
	    				if(obj.code==0){
	    					 var content =  '	<p>添加书本成功!</p>'+
	    					 '			<p>书籍id:'+obj.data.id+'</p>'+
	    					 '			<p>书名：'+obj.data.book_name+'</p>'+
	    					 '			<p>图书位：'+obj.data.book_location+'</p>'+
	    					 '			<p>书本状态： 入库成功</p>'+
	    					 '			<p>书库类：'+obj.data.klassName+'</p>';
	    					 document.getElementById("result").innerHTML = content;
	    						//样式变化
	    						document.getElementById("submitButton").value = "新增图书入库";
	    						document.getElementById("submitButton").disabled=false;
	    						document.getElementById("bookName").value = "";
	    						document.getElementById("ajaxFileinput").innerHTML = '<input type="file" name="file"><br>';
	    				}
	    				else{
	    					 var content =  '	<p style ="color:red">'+obj.msg+'</p>'	
	    					 document.getElementById("result").innerHTML = content;
	    						//样式变化
	    						document.getElementById("submitButton").value = "新增图书入库";
	    						document.getElementById("submitButton").disabled=false;	
	    				}
	                },
	                error:function(e){
	                    alert("错误！！");
	            
	                }
	            });        
		}
	});
</script>
</html>