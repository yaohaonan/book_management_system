<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="./static/js/jquery.js"></script>
<link href="./static/css/menu.css" type="text/css" rel="stylesheet">

<title>全部图书</title>
<style type="text/css">
.books {
	float: left;
	border: 1px solid #CCC;
	margin: 30px 10px;
}

.books:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	-o-transform: scale(1.1);
	-webkit-transition: all .28s ease-in .1s;
	transition: all .28s ease-in .1s;
}
img:hover{
-webkit-transform: scale(1.5);
	-moz-transform: scale(1.5);
	-o-transform: scale(1.5);
-webkit-transition: all .28s ease-in .1s;
	transition: all .28s ease-in .1s;

}
.booksList {
	width: 928px;
	margin:0px auto;
	overflow: hidden;  
}

.bookcontent {
	width: 190px;    
	height: 260px;
	padding: 10px 10px;
	position: relative;
	margin:0px auto;
}

.fontContent {
	margin-top: 20px;
}

.pageButton1 {
	width: 90px;
	font-size: 15px;
	background-color: #009966;
	color: white;
	height: 25px;
}

.pageButton2 {
	width: 90px;
	font-size: 15px;
	background-color: #20B2AA;
	color: white;
	height: 25px;
}
.pageButton3 {
	margin-left:10px;
	width: 90px;
	font-size: 15px;
	background-color: 	#DC143C; 
	color: white;
	height: 25px;
}
.btnButtom {
	position: absolute;
	bottom: 10px;

}

#realodButton {
	width: 50%;
	height: 40px;
	background-color: #F5F5F5;
	border: 1px solid #009966;
	margin-top: 8px;
	margin-bottom: 10px;
}
.onerow {
	width: 120px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap; /*加宽度width属来兼容部分浏览*/
	margin: 0px auto;
}

#searchBooks {
	width: 30%;
	height: 40px;
}

.searchButton {
	width:60px;
	height:40px;
	background-color: #009966;
	color: white;
	margin-bottom: 15px;
}
.searchDiv{
		position: relative;
}
#searchContent{
	width: 29%;   
	margin-top:-10px;  
	text-align: center;
	position: absolute;
	z-index: 100;
	margin-left: 33%;    
	background-color: 	white;          
}
p{
padding-top:4px;
	height: 30px;  
}
p:hover {
	background-color: #CCC;
	cursor:pointer;
}
@media screen and (max-width: 755px){
#searchContent{
	width: 29%;   
	margin-top:-10px;  
	text-align: center;
	position: absolute;
	z-index: 100;
	margin-left: 30%;    
	background-color: 	white;          
}
.booksList {
	width: 500px; 
}
} 
</style>
<script type="text/javascript">
function searchBtn(){
	var book_name = document.getElementById("searchBooks").value;
		if(book_name==""){
				alert("请输入内容");
				return;
		}
		searchForLike(book_name);	
}
</script>
</head>
<body>
<input hidden id="currentPage" value="1">
<h3>
		图书管理系统 -
		全部图书
	</h3>
	<hr>
		<%@include file="./common/menu.jsp"%>

	<div class="buttomDiv" id="ajaxDiv">
	<div class="searchDiv">
		<input type="text" id="searchBooks" placeholder="请您想要查找的书"  oninput="searchBooks()" /> 
		<button  class="searchButton"  onclick="searchBtn()">搜索</button>	
		<div id="searchContent">
			
		</div>  
		</div>
		<div class="booksList" id="booksList">  
		</div>
	<div style="clear: both"></div>
	<button id="realodButton" onclick="reloadAll_book()">加载更多</button>
	</div>

</body>
<script>
	
		function reloadAll_book(){
			var currentPage = document.getElementById("currentPage").value;
			$.post("/library/BookServlet?method=ajax_getAll_book",{"currentPage":currentPage},function(data){		
				console.log(data);
				var obj = eval('(' + data + ')');
				if (obj.code == 0) {
					var dataList = obj.data;
					var allContent = '';
					if(dataList.length<=0){
						document.getElementById("realodButton").innerHTML="已加载全部";
						document.getElementById("realodButton").disabled = true;
					}else{
						for(var i = 0;i<dataList.length;i++){
							var content = '<div class="books" id="'+dataList[i].id+'">'+
							'				<div class="bookcontent">'+
							'					<img src="/library/ImgUrlController?imgurl='+dataList[i].book_img+'" width="80px" height="100px"> '+
							'					<div class="fontContent">'+
							'					<div class="onerow" title="'+dataList[i].book_name+'"> '+'书名：<strong>'+dataList[i].book_name+'</strong></div>'+
							'状态：<span id="book_status_'+dataList[i].id+'">  '+getbook_status(dataList[i].book_status)+'</span>'+ 	
							'   <br> 图书所属类：'+dataList[i].klass_name+' <br>'+
							'  图书位Id：<div id="location_id_'+dataList[i].id+'">'+getbook_location(dataList[i].book_location_id)+' </div><br></div>'+
							'				<div class="btnButtom" id="btnButtom_'+dataList[i].id+'">'+
							getButtonBtn(dataList[i].id,dataList[i].book_status)+
							'					</div>'+
							'				</div>'+
							'			</div>';
							allContent = allContent + content;
						}				
					}

					document.getElementById("currentPage").value = currentPage-0+1
					document.getElementById("booksList").innerHTML = document.getElementById("booksList").innerHTML +allContent;
				}
			});
		}
		reloadAll_book();
		function getbook_status(status) {
			if(status==0){
				return "<strong>已借出</strong>";
			}else{
				return "<strong>在库中</strong>"
			}
		}
		function getbook_location(locationId) {
			if(locationId==null){
				return "<strong>图书已借出不占用图书位</strong>";  
			}else{
				return "<strong>"+locationId+"</strong>";
			}
		}
		
		function alterStatus(book_id,book_status){
			if(book_status==1){
				var r=confirm("您确定要出借吗？");
				if(r){
					$.post("/library/BookServlet?method=ajax_LengBook",{"book_id":book_id,"book_status":book_status},function(data){
						console.log(data);
						var obj = eval('(' + data + ')');
						if(obj.code==0){
								document.getElementById("book_status_"+book_id).innerHTML = getbook_status(0);
								document.getElementById("btnButtom_"+book_id).innerHTML =getButtonBtn(book_id,0);
								document.getElementById("location_id_"+book_id).innerHTML = '<strong>图书已借出不占用图书位</strong>';
						}else{
							alert(obj.msg);
						}
				});
			}
		}else{
			var r=confirm("您确认图书已入库了吗?");
			if(r){   //ajax_intoBook	
				$.post("/library/BookServlet?method=ajax_intoBook",{"book_id":book_id},function(data){
					console.log(data);
					var obj = eval('(' + data + ')');
					if(obj.code==0){
						document.getElementById("book_status_"+book_id).innerHTML = getbook_status(1);
						document.getElementById("btnButtom_"+book_id).innerHTML = getButtonBtn(book_id,1);
						document.getElementById("location_id_"+book_id).innerHTML = '<strong>'+obj.data.localtion_id+'</strong>';
						alert("您已还书成功，需支付金额："+obj.data.money+"元");
					}else{
						alert(obj.msg);
					}
			});		
				}
			}	
		}
		function getButtonBtn(id,status){
				if(status==0){  //已出库
						return '<button  class="pageButton1" onclick="alterStatus(\''+id+'\',\''+status+'\')">点击入库</button>'
						+'<button  class="pageButton3" onclick="deleteBook(\''+id+'\')">删除</button>';	
				}
				else{
					return '<button  class="pageButton2"onclick="alterStatus(\''+id+'\',\''+status+'\')">点击出借</button>'
					+'<button  class="pageButton3" onclick="deleteBook(\''+id+'\')">删除</button>';	
				}
		}
		function searchBooks(){
			var searchContent = document.getElementById("searchBooks").value;
			console.log(searchContent);
			if(searchContent==""|searchContent==null){
				document.getElementById("searchContent").innerHTML = "";
				document.getElementById("currentPage").value = 1;
				document.getElementById("booksList").innerHTML = "";
				reloadAll_book();
				document.getElementById("realodButton").innerHTML="加载更多";
				document.getElementById("realodButton").disabled = false;
			}else{
				$.post("BookServlet?method=ajax_searchBookName",{"book_name":searchContent},function(data){
							console.log(data);
							var obj = eval('(' + data + ')');
							var list = obj.data;
							var allcontent = "";
							for(var i = 0; i<list.length;i++){
									var content = '<p onclick="searchForLike(\''+list[i]+'\')">'+list[i]+'</p>'
									allcontent = allcontent  + content;
							}
							document.getElementById("searchContent").innerHTML = allcontent;
				});	
			}
		}
		
		function searchForLike(book_name){
				$.post("BookServlet?method=searchBookForLike",{"book_name":book_name},function(data){
							console.log(data);
							var obj = eval('(' + data + ')');
							var dataList = obj.data;
							var allContent ="";
							for(var i = 0;i<dataList.length;i++){
								var content = '<div class="books" id="'+dataList[i].id+'">'+
								'				<div class="bookcontent">'+
								'					<img src="/library/ImgUrlController?imgurl='+dataList[i].book_img+'" width="80px" height="100px">'+
								'					<div class="fontContent">'+
								'					<div class="onerow" title="'+dataList[i].book_name+'"> '+'书名：<strong>'+dataList[i].book_name+'</strong></div>'+
								'状态：<span id="book_status_'+dataList[i].id+'">  '+getbook_status(dataList[i].book_status)+'</span>'+ 	
								'   <br> 图书所属类：'+dataList[i].klass_name+' <br>'+
								'  图书位Id：<div id="location_id_'+dataList[i].id+'">'+getbook_location(dataList[i].book_location_id)+' </div><br></div>'+
								'				<div class="btnButtom" id="btnButtom_'+dataList[i].id+'">'+
								getButtonBtn(dataList[i].id,dataList[i].book_status)+
								'					</div>'+
								'				</div>'+
								'			</div>';
								allContent = allContent + content;
							}	
							document.getElementById("booksList").innerHTML = allContent;  
							document.getElementById("realodButton").innerHTML="已到底";
							document.getElementById("realodButton").disabled = true;
							document.getElementById("searchContent").innerHTML="";
							document.getElementById("searchBooks").value=book_name;
				});	
				
		}
		
		function deleteBook(book_id){
			var r=confirm("您确定要从书库中移除该图书吗？");
			if(r){
				$.post("/library/BookServlet?method=ajax_deleteBook",{"book_id":book_id},function(data){
					console.log(data);  
					var obj = eval('(' + data + ')');
					if(obj.code==0){
						var tr = document.getElementById(book_id);
						tr.parentNode.removeChild(tr);
						getSearchBookDetail();
					}else{
						alert(obj.msg);
					}
				});
			}
		}
</script>

</html>