<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
	<link href="./static/css/menu.css" type="text/css" rel="stylesheet">
<script src="./static/js/jquery.js"></script>
<% String book_status  = (String)request.getAttribute("book_status") ;%>
<%if("0".equals(book_status)){
	%>
	<title>图书入库管理</title>
<% 
} else{
	%>
	<title>图书借出管理</title>
<% 
}
%>
<style type="text/css">
.pageButton {
	width: 80px;
	font-size: 15px;
	background-color: #009966;
	color: white;
	height: 25px;
}
.pageButton2 {
	width: 80px;
	font-size: 15px;
	background-color: #99CCCC;
	color: white;
	height: 25px;
}
span {
	font-size: 12px;
	cursor:pointer;
}

table {
	width: 900px;
}

@media screen and (max-width: 755px) {
	table {
		width: 400px;
	}
		*{
		font-size: 12px;
	}
}
</style>
</head>
<body>
	<h3>
		图书管理系统 -
		<%
		if ("0".equals(book_status)) {
	%>
		图书入库管理
		<%
		} else {
	%>
		图书借出管理
		<%
		}
	%>
	</h3>
	<hr>
	<%@include file="./common/menu.jsp"%>

	<input hidden id="currentPage"
		value="<%=(String) request.getAttribute("currentPage")%>">
<input hidden id="book_status"
		value="<%=book_status%>">
	<div class="buttomDiv" id="ajaxDiv"></div>
</body>
<script type="text/javascript">
function getbook_status(status) {
	if(status==0){
		return "已借出";
	}else{
		return "已入库"
	}
}
function bookAction(status,id){
	if(status==0){
		return '点击入库';
	}else{
		return '点击借出';
	}
}
function loadPage(currentPage){   
	document.getElementById("currentPage").value =  currentPage;
	reloadBook_location();
}
function reloadBook_location() {
	var currentPage = document.getElementById("currentPage").value;
	var book_status = document.getElementById("book_status").value;
	$.post("/library/BookServlet?method=ajax_getPageForBook",{"currentPage" : currentPage,"book_status":book_status},
					function(data) {
		console.log(data);
		var obj = eval('(' + data + ')');
		if (obj.code == 0) {
			var listData = obj.data.listData;
			if (listData != null) {
				var alltablesData = "<div id=\"location_number\"></div> ";
				for (var i = 0; i < listData.length; i++) {
					var tableData = '<tr id=\''+listData[i].id+'\'>' 
							+ '  <td>'+ listData[i].rownum + '</td>'
							+ '  <td>' + listData[i].id+ '</td>'
							+ '  <td>'+ listData[i].book_name+ '</td>'
							+ '  <td>'+ getbook_status(listData[i].book_status)+ '</td>'
							+ '  <td>'+ listData[i].klass_name+ '</td>'
							+ '<td><span onclick="deleteBook(\''+ listData[i].id+'\')">删除</span></td>'
							+'<td><span onclick="alterStatus(\''+ listData[i].id+'\',\''+listData[i].book_status+'\')">'+bookAction(listData[i].book_status)+'</span></td></tr>';
					alltablesData = alltablesData+ tableData;
				}
				var ajaxTable = '<table border="1" cellspacing="2" style="margin:0px auto;" class="imagetable">'  
						+ '<tr>'
						+ '  <th></th>'
						+ '  <th>图书id</th>'
						+ '  <th>书名</th>'
						+ '  <th>状态</th>'
						+ '  <th>类名</th>'
						+ '  <th colspan="2">操作</th>'
						+ '</tr>'
						+ alltablesData + '</table>';
						var buttomButton = "";
						if(obj.data.currentPage<=1){
							buttomButton = buttomButton
							+' <input type="button" value="首页"  class="pageButton2" disabled="disabled">'
							+'	<input type="button"value="上一页"  class="pageButton2" disabled="disabled"> ';
						}else{
							buttomButton = buttomButton
							+' <input type="button" value="首页"  class="pageButton" onclick="loadPage('+(1)+')" >'
							+'	<input type="button"value="上一页"  class="pageButton" onclick="loadPage('+(obj.data.currentPage-0-1)+')"> ';
						}
						if(obj.data.currentPage<obj.data.totalPage){
							buttomButton = buttomButton
							+	' <input type="button" value="下一页"  class="pageButton" onclick="loadPage('+(obj.data.currentPage-0+1)+')">'
							+ '<input type="button" value="末页"  class="pageButton" onclick="loadPage('+(obj.data.totalPage)+')">';
						}else{
							buttomButton = buttomButton
							+	' <input type="button" value="下一页"  class="pageButton2" disabled="disabled">'
							+ '<input type="button" value="末页"  class="pageButton2" disabled="disabled">';
						}
						buttomButton = buttomButton +	' <br><br>'+
					'		 <span>第 <span id="startPage">'+ obj.data.currentPage
				+'</span> 页 / 共  <span id="endPage">'+obj.data.totalPage+'</span> 页</span>';
				buttomButton = buttomButton+ '&nbsp;&nbsp;&nbsp;'
				+	'<input type="text" style="width:30px" onblur="searchInput()"  id="searchInput">';
						var allcontent =  ajaxTable +'<br>'+ buttomButton;				
				document.getElementById("ajaxDiv").innerHTML = allcontent;
			
			} else {
				document.getElementById("ajaxDiv").innerHTML = "无数据";
			}
		} else {
			alert(obj.msg);
		}	
	});
	
	setTimeout("getSearchBookDetail()",200); 
}
reloadBook_location();
function searchInput(){	
	var number  = document.getElementById("searchInput").value;
	  var reg = new RegExp("^[0-9]*$");
		if(reg.test(number)){
			if(number>=1){
				document.getElementById("currentPage").value =  document.getElementById("searchInput").value;
				reloadBook_location();
			}	
		}else{
			console.log("请输入正确数字");
		}	
	}
	function deleteBook(book_id){
		var r=confirm("您确定要从书库中移除该图书吗？");
		if(r){
			$.post("/library/BookServlet?method=ajax_deleteBook",{"book_id":book_id},function(data){
				console.log(data);  
				var obj = eval('(' + data + ')');
				if(obj.code==0){
					var tr = document.getElementById(book_id);
					tr.parentNode.removeChild(tr);
					getSearchBookDetail();
				}else{
					alert(obj.msg);
				}
			});
		}
	}
	function alterStatus(book_id,book_status){
		if(book_status==1){
			var r=confirm("您确定要出借吗？");
			if(r){
				$.post("/library/BookServlet?method=ajax_LengBook",{"book_id":book_id,"book_status":book_status},function(data){
					console.log(data);
					var obj = eval('(' + data + ')');
					if(obj.code==0){
						var tr = document.getElementById(book_id);
						tr.parentNode.removeChild(tr);
						getSearchBookDetail();
					}else{
						alert(obj.msg);
					}
			});
		}
	}else{
		var r=confirm("您确认图书已入库了吗?");
		if(r){   //ajax_intoBook	
			$.post("/library/BookServlet?method=ajax_intoBook",{"book_id":book_id},function(data){
				console.log(data);
				var obj = eval('(' + data + ')');
				if(obj.code==0){
					var tr = document.getElementById(book_id);
					tr.parentNode.removeChild(tr);
					getSearchBookDetail();
					var objdata = obj.data;
					alert("您已还书成功，需支付金额："+objdata.money+"元");
				}else{
					alert(obj.msg);
				}
		});
			
			
			}
		}	
	}
function getSearchBookDetail(){
	$.post("/library/BookServlet?method=ajax_searchBookDetail",{},function(data){
		console.log(data);  
		var obj = eval('(' + data + ')');
		if(obj.code==0){
			document.getElementById("location_number").innerHTML = '  '+'<p>A类书有'+obj.data.a_klass
				+'本,B类书有'+obj.data.b_klass
				+'本,入库有'+obj.data.book_in+'本,出借有'+obj.data.book_out+"本，全部有"+obj.data.total+"本</p>";
				;
		}else{
			document.getElementById("location_number").innerHTML = obj.msg;
		}	
	});
	
}	
</script>


</html>