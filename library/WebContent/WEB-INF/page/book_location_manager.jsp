<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
<title><%=(String) request.getAttribute("klassName")%>类图书位管理</title>
<link href="./static/css/menu.css" type="text/css" rel="stylesheet">
<script src="./static/js/jquery.js"></script>
<style type="text/css">
.pageButton {
	width: 80px;
	font-size: 15px;
	background-color: #009966;
	color: white;
	height: 25px;
}
.pageButton2 {
	width: 80px;
	font-size: 15px;
	background-color: #99CCCC;
	color: white;
	height: 25px;
}
span {
	font-size: 12px;
	cursor:pointer;
}

table {
	width: 700px;
}

@media screen and (max-width: 755px) {
	table {
		width: 400px;
	}
}
</style>
</head>
<body>
	<h3>
		图书管理系统 -
		<%=(String) request.getAttribute("klassName")%>类图书位管理
	</h3>
	<hr>
	<%@include file="./common/menu.jsp"%>

	<input hidden id="currentPage"
		value="<%=(String) request.getAttribute("currentPage")%>">
	<input hidden id="klassName"
		value="<%=(String) request.getAttribute("klassName")%>">

	<div class="buttomDiv" id="ajaxDiv"></div>
</body>
<script type="text/javascript">
	function location_status(status) {  
		if(status==0){  		
			return "空闲状态";
		}else{
			return "已占用" 
		}
}
function reloadBook_location() {
	var currentPage = document.getElementById("currentPage").value;
	var klassName = document.getElementById("klassName").value;
	$.post("/library/Book_LocationServlet?method=ajax_book_location",{"currentPage" : currentPage,"klassName" : klassName},
					function(data) {
						var obj = eval('(' + data + ')');
						if (obj.code == 0) {
							var listData = obj.data.listData;
							if (listData != null) {
								var alltablesData = "<div id=\"location_number\"></div> ";
								for (var i = 0; i < listData.length; i++) {
									var tableData = '<tr id=\''+listData[i].id+'\'>' + '  <td>'
											+ listData[i].rownum + '</td>'
											+ '  <td>' + listData[i].id
											+ '</td>' + '  <td>'
											+location_status(listData[i].location_status)
											+ '</td>' + '<td><span onclick="deleteBookLocation(\''+ listData[i].id+'\')">删除</span></td>'
											+'<td><span onclick="alterKlass(\''+ listData[i].id+'\')">换类</span></td></tr>';
									alltablesData = alltablesData
											+ tableData;
								}
								var ajaxTable = '<table border="1" cellspacing="2" style="margin:0px auto;" class="imagetable">'
										+ '<tr>'
										+ '  <th></th>'
										+ '  <th>图书位id</th>'
										+ '  <th>位置状态</th>'
										+ '  <th colspan="2">操作</th>'
										+ '</tr>'
										+ alltablesData + '</table>';
										var buttomButton = "";
										if(obj.data.currentPage<=1){
											buttomButton = buttomButton
											+' <input type="button" value="首页"  class="pageButton2" disabled="disabled">'
											+'	<input type="button"value="上一页"  class="pageButton2" disabled="disabled"> ';
										}else{
											buttomButton = buttomButton
											+' <input type="button" value="首页"  class="pageButton" onclick="loadPage('+(1)+')" >'
											+'	<input type="button"value="上一页"  class="pageButton" onclick="loadPage('+(obj.data.currentPage-0-1)+')"> ';
										}
										if(obj.data.currentPage<obj.data.totalPage){
											buttomButton = buttomButton
											+	' <input type="button" value="下一页"  class="pageButton" onclick="loadPage('+(obj.data.currentPage-0+1)+')">'
											+ '<input type="button" value="末页"  class="pageButton" onclick="loadPage('+(obj.data.totalPage)+')">';
										}else{
											buttomButton = buttomButton
											+	' <input type="button" value="下一页"  class="pageButton2" disabled="disabled">'
											+ '<input type="button" value="末页"  class="pageButton2" disabled="disabled">';
										}
										buttomButton = buttomButton +	' <br><br>'+
									'		 <span>第 <span id="startPage">'+ obj.data.currentPage
								+'</span> 页 / 共  <span id="endPage">'+obj.data.totalPage+'</span> 页</span>';
								buttomButton = buttomButton+ '&nbsp;&nbsp;&nbsp;'
								+	'<input type="text" style="width:30px" onblur="searchInput()"  id="searchInput">';
										var allcontent =  ajaxTable +'<br>'+ buttomButton;				
								document.getElementById("ajaxDiv").innerHTML = allcontent;
							} else {
								document.getElementById("ajaxDiv").innerHTML = "无数据";
							}
						} else {
							alert(obj.msg);
						}
						console.log(data);
					});
	
	setTimeout("ajax_getLocationNumber()",200); 
		
}
reloadBook_location();
function loadPage(currentPage){   
	document.getElementById("currentPage").value =  currentPage;
	reloadBook_location();
}
function searchInput(){	
var number  = document.getElementById("searchInput").value;
  var reg = new RegExp("^[0-9]*$");
	if(reg.test(number)){
		if(number>=1){
			document.getElementById("currentPage").value =  document.getElementById("searchInput").value;
			reloadBook_location();
		}	
	}else{
		console.log("请输入正确数字");
	}	
}
function deleteBookLocation(location_id){
var r=confirm("您确定要删除该图书位吗？");
if(r){
	$.post("/library/Book_LocationServlet?method=delete_book_location",{"location_id":location_id},function(data){
		console.log(data);  
		var obj = eval('(' + data + ')');
		if(obj.code==0){
			var tr = document.getElementById(location_id);
			tr.parentNode.removeChild(tr);
			ajax_getLocationNumber();
		}else{
			alert(obj.msg);
		}
	});
}
}

function ajax_getLocationNumber(){
var klassName = document.getElementById("klassName").value;
$.post("/library/Book_LocationServlet?method=ajax_getLocationNumber",{"klassName":klassName},function(data){
	console.log(data);
	var obj = eval('(' + data + ')');
	if(obj.code==0){
		document.getElementById("location_number").innerHTML = '  '+'<p>该类图书位总数为'+obj.data.total
			+'个,空闲为'+obj.data.remain
			+'个,已占用'+obj.data.occupy+'个 <button class="pageButton" onclick="addLocation()">新增图书位</button></p>';
			;
	}else{
		document.getElementById("location_number").innerHTML = obj.msg;
	}
	
}); 
}

function addLocation(){
var number=prompt("请输入您想要增加的图书位的个数");
 var reg = new RegExp("^[0-9]*$");
 	if(number!=""){
 		if(reg.test(number)){
 			if(number<200){
 				var klassName = document.getElementById("klassName").value;
 				$.post("/library/Book_LocationServlet?method=ajax_addBook_location",{"klassName":klassName,"number":number},function(data){
 							console.log(data);
 							var obj = eval('(' + data + ')');
 							if(obj.code==0){
 								reloadBook_location();
 							}
 				});
 			}else{
 				alert("你要搞垮我吗？");
 			}		
			
	}else{
		console.log("请输入正确数字");
		}	
	}
}
function alterKlass(location_id){
var klassName=prompt("您打算转到什么类呢?");
 	if(klassName!=null){
			$.post("/library/Book_LocationServlet?method=ajax_alterBook_klass",{"klassName":klassName,"location_id":location_id},function(data){
						console.log(data);
						var obj = eval('(' + data + ')');
						if(obj.code==0){
							reloadBook_location();
						}else{
							alert(obj.msg);
						}
	});	
}
}


</script>
</html>