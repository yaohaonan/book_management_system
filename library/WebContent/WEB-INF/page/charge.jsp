<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
<meta name="format-detection" content="telephone=no" />
	<link href="./static/css/menu.css" type="text/css" rel="stylesheet">
<script src="./static/js/jquery.js"></script>
<title>图书收费明细</title>
<style type="text/css">
.pageButton {
	width: 80px;
	font-size: 15px;
	background-color: #009966;
	color: white;
	height: 25px;
}
.pageButton2 {
	width: 80px;
	font-size: 15px;
	background-color: #99CCCC;
	color: white;
	height: 25px;
}
span {
	font-size: 12px;
	cursor:pointer;
}

table {
	width: 90%; 
}

@media screen and (max-width: 755px) {
	table {
		width: 400px;
	}
}
</style>
</head>

<body>	
<h3>
		图书管理系统 - 图书收费明细
	</h3>
	<hr>
<%@include file="./common/menu.jsp"%>
	<input hidden id="currentPage"
		value="<%=(String) request.getAttribute("currentPage")%>">
<div class="buttomDiv" id="ajaxDiv"></div>
</body>
	<script type="text/javascript">
	function loadPage(currentPage){   
		document.getElementById("currentPage").value =  currentPage;
		reloadBook_history();
	}
	function getbook_status(status) {
		if(status==0){
			return "图书借出";
		}else{
			return "图书入库"
		}
	}
    function getDateDiff(dateTimeStamp){
        var minute = 1000 * 60;
        var hour = minute * 60;
        var day = hour * 24;
        var halfamonth = day * 15;
        var month = day * 30;
        var now = new Date().getTime();
        var diffValue = now - dateTimeStamp;
        if(diffValue < 0){return;}
        var monthC =diffValue/month;
        var weekC =diffValue/(7*day);
        var dayC =diffValue/day;
        var hourC =diffValue/hour;
        var minC =diffValue/minute;
        if(monthC>=1){
            result="" + parseInt(monthC) + "月前";
        }
        else if(weekC>=1){
            result="" + parseInt(weekC) + "周前";
        }
        else if(dayC>=1){
            result=""+ parseInt(dayC) +"天前";
        }
        else if(hourC>=1){
            result=""+ parseInt(hourC) +"小时前";
        }
        else if(minC>=1){
            result=""+ parseInt(minC) +"分钟前";
        }else
            result="刚刚";
        return result;
    }
    function getLocalTime(nS) { 
    	var date = new Date(nS);
    	Y = date.getFullYear() + '-';
    	M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    	D = date.getDate() + ' ';
    	h = date.getHours() + ':';
    	m = date.getMinutes() + ':';
    	s = date.getSeconds();
    	console.log(Y+M+D+h+m+s); //呀麻碟
    }
	function reloadBook_history() {
		var currentPage = document.getElementById("currentPage").value;
		$.post("/library/ajax_getBook_charge",{"currentPage" : currentPage},
						function(data) {
			console.log(data);
			var obj = eval('(' + data + ')');
			if (obj.code == 0) {
				var listData = obj.data.listData;
				if (listData != null) {
					var alltablesData = "<div id=\"location_number\"></div> ";
					for (var i = 0; i < listData.length; i++) {
						var tableData = '<tr>' 
								+ '  <td>'+ listData[i].rownum + '</td>'
								+ '  <td>' + listData[i].book_name+ '</td>'
								+ '  <td>'+  listData[i].money+ '元</td>'
								+ '  <td>'+ listData[i].book_out_time+ '</td>'
								+ '  <td>'+listData[i].book_in_time+ '</td>'
								+ '  <td>'+ listData[i].location_number+ '</td>'
								+'</tr>';
						alltablesData = alltablesData+ tableData;
					}  
					var ajaxTable = '<table border="1" cellspacing="2" style="margin:0px auto;" class="imagetable">'
							+ '<tr>'
							+ '  <th></th>'
							+ '  <th>书名</th>'
							+ '  <th>消费金额</th>'
							+ '  <th>借书时间</th>'
							+ '  <th>还书时间</th>'
							+ '  <th>图书位id</th>'
							+ '</tr>'
							+ alltablesData + '</table>';
							var buttomButton = "";
							if(obj.data.currentPage<=1){
								buttomButton = buttomButton
								+' <input type="button" value="首页"  class="pageButton2" disabled="disabled">'
								+'	<input type="button"value="上一页"  class="pageButton2" disabled="disabled"> ';
							}else{
								buttomButton = buttomButton
								+' <input type="button" value="首页"  class="pageButton" onclick="loadPage('+(1)+')" >'
								+'	<input type="button"value="上一页"  class="pageButton" onclick="loadPage('+(obj.data.currentPage-0-1)+')"> ';
							}
							if(obj.data.currentPage<obj.data.totalPage){
								buttomButton = buttomButton
								+	' <input type="button" value="下一页"  class="pageButton" onclick="loadPage('+(obj.data.currentPage-0+1)+')">'
								+ '<input type="button" value="末页"  class="pageButton" onclick="loadPage('+(obj.data.totalPage)+')">';
							}else{
								buttomButton = buttomButton
								+	' <input type="button" value="下一页"  class="pageButton2" disabled="disabled">'
								+ '<input type="button" value="末页"  class="pageButton2" disabled="disabled">';
							}
							buttomButton = buttomButton +	' <br><br>'+
						'		 <span>第 <span id="startPage">'+ obj.data.currentPage
					+'</span> 页 / 共  <span id="endPage">'+obj.data.totalPage+'</span> 页</span>';
					buttomButton = buttomButton+ '&nbsp;&nbsp;&nbsp;'
					+	'<input type="text" style="width:30px" onblur="searchInput()"  id="searchInput">';
							var allcontent =  ajaxTable +'<br>'+ buttomButton;				
					document.getElementById("ajaxDiv").innerHTML = allcontent;
				
				} else {
					document.getElementById("ajaxDiv").innerHTML = "无数据";
				}
			} else {
				alert(obj.msg);
			}	
		});
	
	}
	reloadBook_history();
	function searchInput(){	
		var number  = document.getElementById("searchInput").value;
		  var reg = new RegExp("^[0-9]*$");
			if(reg.test(number)){
				if(number>=1){
					document.getElementById("currentPage").value =  document.getElementById("searchInput").value;
					reloadBook_history();
				}	
			}else{
				console.log("请输入正确数字");
			}	
		}
	</script>	
		
		
</html>