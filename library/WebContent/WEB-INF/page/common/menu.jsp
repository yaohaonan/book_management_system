<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<script>
      $(function(){
        //水平导航栏，鼠标经过下拉导航栏
        $(".hmain").hover(function(){
          $(this).find(".child").stop(true).slideToggle(400)
            .parent().siblings().find(".child").slideUp();  
        })
      }) 
    </script>
	<div>
		<ul class="content" >
			<li class="hmain"><a href="#">图书入库与借出</a>
				<ul class="child">
					<li><a href="/library/JspServlet?method=bookStatus&book_status=0">图书入库</a></li>
					<li><a href="/library/JspServlet?method=bookStatus&book_status=1">图书借出</a></li>
				</ul></li> 
			<li class="hmain"><a href="#">图书位管理</a>
				<ul class="child">
					<li><a href="/library/JspServlet?method=reloadBook_location_manager&klassName=A">A类图书位</a></li>
					<li><a href="/library/JspServlet?method=reloadBook_location_manager&klassName=B">B类图书位</a></li>
				</ul></li>  
			<li class="hmain"><a href="#">图书库记录</a>
				<ul class="child">
					<li><a href="/library/JspServlet?method=book_History">进出图书库历史记录</a></li>
				</ul></li>
			<li class="hmain"><a href="#">财务</a>
				<ul class="child">
					<li><a href="/library/JspServlet?method=book_charge">图书收费明细</a></li>
				</ul></li>
			<li class="hmain"><a href="#">图书管理</a>
				<ul class="child"> 
					<li><a href="/library/JspServlet?method=add_book">新增图书</a></li>
					<li><a href="/library/JspServlet?method=allBook">全部图书</a></li>
				</ul>
			</li> 
		</ul>
	</div>