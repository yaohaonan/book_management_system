<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
<meta name="format-detection" content="telephone=no" />
<title>首页</title>
<script src="./static/js/jquery.js"></script>
<link href="./static/css/menu.css" type="text/css" rel="stylesheet">
</head>
<body>
	<h3>欢迎进入 图书管理系统</h3>
	<hr>
  <%@include file="./common/menu.jsp"%>
</body>
</html>
