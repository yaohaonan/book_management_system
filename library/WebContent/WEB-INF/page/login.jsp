<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登陆界面</title>
<script src="./static/js/jquery.js"></script>
<script src="./static/js/md5.js"></script>
<style type="text/css">
.loginDiv{
		width: 450px;
		border: 1px solid #CCC;
		border-radius: 2%;
		margin: 10% auto;
}	
h3{
	text-align: center;
}
input {
	margin: 8px 3px;
	height: 30px;
	font-size: 15px;
	background-color: #F5FFFA
}
#submitButton{
	width: 250px;
	font-size:15px;
	background-color: 	#2E8B57;
	color: white;
	height: 35px;
}
#submitButton:hover{
	background-color: 	#008B8B;
}
.formDiv{
	text-align: center;
}
body {
	background-color: #F5F5F5
}
</style>
</head>
<body>
	<%
		String salt = (String) request.getAttribute("salt");
	%>
	<input hidden id="salt" value="<%=salt%>">
	<div>
	<div class="loginDiv">
	<h3>欢迎 图书管理系统 注册</h3>
	<div class= "formDiv">
		<form action="/library/UserLoginServlet?method=userLogin" method="post"
			onsubmit="return checkForm()">
			用户名：<input type="text" name="username" id="username" onkeyup="keyDownCheck('username')"  placeholder="请输入用户名"/> 
			<br><span
				style="color: red;d" id="username_error" ></span><br>
			密码:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="password" name="password"
					id="password"  placeholder="请输入密码" onkeyup="keyDownCheck('password')">
				<br> <span style="color: red" id="password_error"></span><br>
			<div style="margin-right: 0px">
				<input type="submit" value="点击登陆" id="submitButton">
			</div>
		</form>
		<span style="color: #CCC">come from yhn</span>
		</div>
	</div>
	</div>
	<script type="text/javascript">
	function keyDownCheck(inputId) {
		var input_  =  document.getElementById(inputId);
		if(input_.value!=""){
			input_.style= "border: 1px solid #CCC;";
			document.getElementById(inputId+"_error").innerHTML = "";
		}
	}
		function checkForm() {
			var username = document.getElementById("username").value;
			var password = document.getElementById("password").value; 
			var flat = true;
			if (username == "") {
				document.getElementById("username_error").innerHTML = "您输入的用户名为空";
				document.getElementById("username").style= "border: 1px solid red;";
				flat = false;
			}
			if (password == "") {
				document.getElementById("password_error").innerHTML = "您输入的密码为空";
				document.getElementById("password").style= "border: 1px solid red;";
				flat = false;
			}
			if (flat == true) { //直接return flat会被当字符...
					var hex_md5Passowrd =hex_md5(password+document.getElementById("salt").value);
					document.getElementById("password").value = hex_md5Passowrd;	
			
				return true;
			} else {
				return false;
			}  
		}
	</script>
</body>
</html>