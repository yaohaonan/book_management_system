package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OracleConfig {
	public static final String USERNAME = "scott";
	public static final String PASSWORD = "nannima";
	public static final String JDBCURL = "jdbc:oracle:thin:@127.0.0.1:1521:orcl";
	public static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
//	//注册数据库驱动
	static {
			try {
				Class.forName(OracleConfig.DRIVER);// 加载Oracle驱动程序
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	//获取数据库连接
	public static Connection getConnection() {
		try {
			return DriverManager.getConnection(JDBCURL, USERNAME, PASSWORD);
		} catch (Exception e) {
			return null;
		}
	}
	public static void release(Connection conn,PreparedStatement pre ,ResultSet result) {
			if(result!=null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}finally {
					result=null;
				}
			}
			if(pre!=null) {
				try {
					pre.close();
				} catch (Exception e) {
					e.printStackTrace();
				}finally {
					pre=null;
				}
				if(conn!=null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}finally {
						conn=null;
					}
					
				}
			}
	}	
}
