package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

import service.BookService;
import service.serviceImpl.BookServiceImpl;
import util.ResultVOUtil;
import vo.ResultVO;

/**
 * Servlet implementation class Ajax_reloadHistory
 */
@WebServlet("/ajax_reloadHistory")
public class Ajax_reloadHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;
      private BookService bookService = new BookServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ajax_reloadHistory() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");	
		String currentPage = request.getParameter("currentPage");
		ResultVO resultVO = ResultVOUtil.success(bookService.getPageForBook_history(currentPage));
		String json = JSON.toJSONString(resultVO);
		PrintWriter pw = response.getWriter();
        pw.write(json);
        pw.flush();
        pw.close();
	}

}
