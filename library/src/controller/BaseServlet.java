package controller;
import java.io.IOException;
import java.lang.reflect.Method;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//这个抽象类，BaseServlet类不需要在web.xml中进行配置
public abstract class BaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

// final 防子类复写
  public final void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
	  System.out.println("这是一个Get请求");
	  doPost(request, response);
  }

  public final void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
	  request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");	
      // 1、获得执行的方法名
      String methodName = request.getParameter("method");
      // 默认方法
      if (methodName == null) {
          methodName = "execute";
      }
      System.out.println("BaseServlet : " + this + " , " + methodName);
      try {
          // 2、通过反射获得当前运行类中指定方法,形式参数
          Method executeMethod = this.getClass().getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
          // 3、反射执行方法
          Object obj = executeMethod.invoke(this, request, response);
          if(obj==null) {
        	        //如果对象为空表示无返回值，无返回值则默认认为是跳转页面（则不进行任何操作）
          }else {
        	  String result = (String)obj;
        	    //将json数据返回
              response.getWriter().write(result);
          }
      } catch (NoSuchMethodException e) {
          throw new RuntimeException("请求的方法[" + methodName + "]不存在");
      } catch (Exception e) {
          e.printStackTrace();
          throw new RuntimeException("服务器异常", e);
      }
  }

  /**
   * 此方法用于复写，方便子类编程，默认执行方法
   */
  public void execute(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
	  
	  
  }
}
