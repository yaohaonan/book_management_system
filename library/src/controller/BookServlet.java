package controller;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import com.alibaba.fastjson.JSON;
import config.ProjectConfig;
import enums.ResultEnum;
import service.BookService;
import service.serviceImpl.BookServiceImpl;
import util.FileSaveLocalUtil;
import util.KeyUtil;
import util.ResultVOUtil;
import vo.ResultVO;
@WebServlet("/BookServlet")
@MultipartConfig
public class BookServlet extends BaseServlet{
	private BookService bookService = new BookServiceImpl();
	private static final long serialVersionUID = 1L;
	public String ajax_addBook(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		ResultVO result = null;
		String bookName = request.getParameter("bookName");
		String klassName = request.getParameter("klassName");
		Part part = request.getPart("file");
		System.out.println(part.getContentType()); 
		if(part.getSize()>1024*1024*2|part.getSize()<=0) {   //限制图片为2M内
			 System.err.println("文件大小不符合系统要求");
			 result =  ResultVOUtil.error(ResultEnum.BOOK_IMG_SIZE.getCode(), ResultEnum.BOOK_IMG_SIZE.getMessage());
			
		}
		else if("image/png".equals(part.getContentType())|"application/x-png".equals(part.getContentType())
				|"image/jpeg".equals(part.getContentType())|"application/x-jpg".equals(part.getContentType())){		
			String fileName = KeyUtil.genUniqueKey();
			FileSaveLocalUtil.savePic(part.getInputStream(),ProjectConfig.FILE_PATH, fileName);
			if("".equals(bookName.trim())|bookName==null) {
				result =  ResultVOUtil.error(ResultEnum.BOOK_NAME_NULL.getCode(), ResultEnum.BOOK_NAME_NULL.getMessage());
			}else {
				result  = bookService.saveBook(bookName, klassName,fileName);
			}
		}
		else {
			System.err.println("文件类型不符合系统要求");
			 result =  ResultVOUtil.error(ResultEnum.BOOK_IMG_TYPE.getCode(), ResultEnum.BOOK_IMG_TYPE.getMessage());
		}	
		String json = JSON.toJSONString(result);
		return json;
	}
	public String ajax_deleteBook(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {		
		String book_id = 	request.getParameter("book_id");
		ResultVO resultVO = bookService.deleteBook(book_id);
		String json = JSON.toJSONString(resultVO);
		return json;
	}
	public String ajax_getAll_book(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String currentPage =request.getParameter("currentPage");
		ResultVO resultVO = bookService.getPageForAll_book(currentPage);
		String json = JSON.toJSONString(resultVO);
		return json;
	}
	public String ajax_getPageForBook(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
			String book_status= 	request.getParameter("book_status");
			String currentPage = request.getParameter("currentPage");
			ResultVO resultVO = 	bookService.getPageForBook(book_status, currentPage);
			String json = JSON.toJSONString(resultVO);
			return json;
	}
	public String ajax_intoBook(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {	
		String book_id = request.getParameter("book_id");
		ResultVO resultVO = bookService.intoBook(book_id);
		String json = JSON.toJSONString(resultVO);
			return json;
	}
	public String ajax_LengBook(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String book_id = request.getParameter("book_id");
		String book_status = request.getParameter("book_status");
		ResultVO resultVO = bookService.lendBook(book_id, book_status);
		String json = JSON.toJSONString(resultVO);
		return json;
	}
	public String ajax_searchBookDetail(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {	
		String json = JSON.toJSONString(bookService.searchBookDetail());
		return json;
	}
	public String ajax_searchBookName(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {	
		      String book_name = request.getParameter("book_name");
		      if(book_name.isEmpty()) {      
		    	  	return JSON.toJSONString(ResultVOUtil.success());   //不查询，直接返回空
		      }
		      return JSON.toJSONString(bookService.searchBook(book_name));	
	}
	
	public String searchBookForLike(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {	
	      String book_name = request.getParameter("book_name");
	      if(book_name.isEmpty()) {      
	    	  	return JSON.toJSONString(ResultVOUtil.success());   //不查询，直接返回空
	      }
	      return JSON.toJSONString(bookService.searchBookForLike(book_name));	
	}
	
	
}
