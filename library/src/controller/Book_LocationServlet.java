package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

import service.BookService;
import service.serviceImpl.BookServiceImpl;
import util.ResultVOUtil;
import vo.PageVO;
import vo.ResultVO;

@WebServlet("/Book_LocationServlet")
public class Book_LocationServlet extends BaseServlet{
	private BookService  bookService= new BookServiceImpl();
	public String ajax_addBook_location(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String klassName = request.getParameter("klassName");
		String number = request.getParameter("number");
		String json = JSON.toJSONString(bookService.addBook_location(klassName, number));
		return json;
	}
	public String ajax_alterBook_klass(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String klassName = request.getParameter("klassName");
		String location_id = request.getParameter("location_id");
		ResultVO resultVO = bookService.alterKlass_id(klassName, location_id);
		String json = JSON.toJSONString(resultVO);
		return json;
	}
	//异步分页得到图书位
	public String ajax_book_location(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String currentPage =request.getParameter("currentPage");
		String klassName = request.getParameter("klassName");
		String klassId = bookService.findByName(klassName).getId();
		PageVO pageVO = 	bookService.getPageForBook_Location(klassId,currentPage);		
		String json = JSON.toJSONString(ResultVOUtil.success(pageVO));
		return json;
	}
		public String delete_book_location(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
			String location_id = request.getParameter("location_id");
			String json = JSON.toJSONString(bookService.deleteLocation(location_id));
			return json;
		}

		public String ajax_getLocationNumber(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
			String klassName = 	request.getParameter("klassName");
			ResultVO resultVO = bookService.getNumberForLocation(klassName);
			String json = JSON.toJSONString(resultVO);
			return json;
		}
}
