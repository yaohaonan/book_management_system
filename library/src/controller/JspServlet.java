package controller;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.BookService;
import service.serviceImpl.BookServiceImpl;
@WebServlet("/JspServlet")   
public class JspServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private BookService bookServiceImpl = new BookServiceImpl();
	public  void add_book(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("book_klassList", bookServiceImpl.getBook_klass());
		request.getRequestDispatcher("/WEB-INF/page/addBook.jsp").forward(request, response);
	}
	public  void allBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("currentPage", "1");   //第一次访问页面都为第一页开始
		request.getRequestDispatcher("/WEB-INF/page/allBook.jsp").forward(request, response);
	}
	public  void book_charge(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("currentPage", "1");   //第一次访问页面都为第一页开始
		request.getRequestDispatcher("/WEB-INF/page/charge.jsp").forward(request, response);
	}
	public  void book_History(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("currentPage", "1");   //第一次访问页面都为第一页开始
		request.getRequestDispatcher("/WEB-INF/page/history.jsp").forward(request, response);
	}
	public  void bookStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String book_status = request.getParameter("book_status");
		request.setAttribute("book_status",book_status );
		request.setAttribute("currentPage", "1");   //第一次访问页面都为第一页开始
		request.getRequestDispatcher("/WEB-INF/page/book.jsp").forward(request, response);
	}
	public  void reloadBook_location_manager(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String klassName = req.getParameter("klassName");  //类名
		req.setAttribute("currentPage", "1");   //第一次访问页面都为第一页开始
		req.setAttribute("klassName",klassName); 
		req.getRequestDispatcher("/WEB-INF/page/book_location_manager.jsp").forward(req, resp);
	}
}
