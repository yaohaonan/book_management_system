package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import config.ProjectConfig;
import entity.User_;
import enums.ResultEnum;
import service.BookService;
import service.UserService;
import service.serviceImpl.BookServiceImpl;
import service.serviceImpl.UserServiceImpl;

@WebServlet("/UserLoginServlet")   
public class UserLoginServlet extends BaseServlet{
	private static final long serialVersionUID = 1L;
	private UserService userServiceImpl = new UserServiceImpl();
	 //�û���½
		public  void userLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			System.out.println(username+":::"+password);
			User_ user = userServiceImpl.login(username, password);
			if(user==null) {
				request.setAttribute("salt", ProjectConfig.SALT);  //��½��ֵ
				request.getRequestDispatcher("/WEB-INF/page/login.jsp").forward(request, response);
			}else {
				request.getSession().setAttribute("user", user);  //����session
				request.getRequestDispatcher("/WEB-INF/page/allBook.jsp").forward(request, response);
			}  
		}
		
		public  void userLoginForward(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			request.setAttribute("salt", ProjectConfig.SALT);  //��½��ֵ
			request.getRequestDispatcher("/WEB-INF/page/login.jsp").forward(request, response);
		}
}
