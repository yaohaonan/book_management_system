package dao;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import config.OracleConfig;
import config.ProjectConfig;
import dto.BookDetailDTO;
import dto.BookStatusDTO;
import dto.IntoBookDTO;
import entity.Book;
import enums.Book_status_Enum;
import enums.Del_flagEnum;
import enums.ResultEnum;
import util.KeyUtil;
import util.ResultVOUtil;
import util.TimeForMoney;
import vo.PageVO;
import vo.ResultVO;
public class BookDao {
	private Book_historyDao book_historyDao = new Book_historyDao();
	public synchronized Book save(String bookName, String book_location_id, String book_klass_id,String fileName) { // 保存书籍入库
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		PreparedStatement pre2 = null;
		ResultSet result = null;// 创建一个结果集对象
		try {
			con = OracleConfig.getConnection();
			con.setAutoCommit(false); // 禁止自动提交(事务提交)
			// save Book
			String sql1 = "insert into book(id,book_name,book_status,book_location_id,book_klass_id,del_flag,book_img) values (?,?,?,?,?,?,?)";
			pre1 = con.prepareStatement(sql1);// 实例化预编译语句
			String book_id = KeyUtil.genUniqueKey();
			pre1.setString(1, book_id);// 设置参数，前面的1表示参数的索引，而不是表中列名的索引
			pre1.setString(2, bookName);
			pre1.setString(3, Book_status_Enum.BOOK_IN.getCode());
			pre1.setString(4, book_location_id);
			pre1.setString(5, book_klass_id);
			pre1.setString(6, Del_flagEnum.EXIST.getCode());
			pre1.setString(7, fileName);
			pre1.executeQuery();// 执行查询，注意括号中不需要再加参数
			//update 
			String sql2 = "update book_location set location_status = ? where  id = ?";
			pre2 = con.prepareStatement(sql2);// 实例化预编译语句
			pre2.setString(1, Book_status_Enum.BOOK_IN.getCode());
			pre2.setString(2, book_location_id);
			pre2.executeQuery();
			con.commit();
			// 设置返回对象
			Book book = new Book();
			book.setId(book_id);
			book.setBook_klass_id(book_klass_id);
			book.setBook_name(bookName);
			book.setBook_status(Book_status_Enum.BOOK_IN.getCode());
			book.setBook_location_id(book_location_id);
			return book;
		} catch (Exception e) {
			try {
				con.rollback();
				System.err.println("图书入库失败，事务已回滚");
			} catch (SQLException e1) {	
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
				OracleConfig.release(con, pre1, result);
				OracleConfig.release(con, pre2, result);
		}
		return null;
	}
	
	public PageVO<BookStatusDTO> getBookForStauts(String book_status,String currentPage) {   //根据图书状态分页得到图书数据
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;
		PreparedStatement pre2 = null;
		ResultSet result2 = null;
		try {
			con = OracleConfig.getConnection();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("select a1.* from ");
			stringBuilder.append("(select id,book_name,book_status,book_img,"); 
			stringBuilder.append("(select name from book_klass where book_klass.id = b.book_klass_id) as klassName,");
			stringBuilder.append("rownum rn from book b where b.del_flag=? and b.book_status = ?) ");
			stringBuilder.append("a1 where rn between ? and ?");
			pre1 = con.prepareStatement(stringBuilder.toString());// 实例化预编译语句
			pre1.setString(1, Del_flagEnum.EXIST.getCode());
			pre1.setString(2,book_status );
			int startLocated = ((Integer.valueOf(currentPage)-1)*ProjectConfig.PAGE_SIZE)+1;
			int endLocated =Integer.valueOf(currentPage)* ProjectConfig.PAGE_SIZE;
			pre1.setString(3,String.valueOf(startLocated));
			pre1.setString(4,String.valueOf(endLocated));
			System.out.println("开始："+startLocated+"，结束："+endLocated);
			result = pre1.executeQuery();  
			List<BookStatusDTO> books = new ArrayList<BookStatusDTO>();
			
			while(result.next()) {
				BookStatusDTO bookStatusDTO = new BookStatusDTO();
				bookStatusDTO.setBook_name(result.getString("book_name"));
				bookStatusDTO.setBook_status(result.getString("book_status"));
				bookStatusDTO.setId(result.getString("id"));
				bookStatusDTO.setKlass_name(result.getString("klassName"));
				bookStatusDTO.setRownum(result.getString("rn"));
				bookStatusDTO.setBook_img(result.getString("book_img"));
				books.add(bookStatusDTO);
			}  
			String sql2 = "select count(*) count from book where book_status = ? and del_flag = ?";
			pre2 = con.prepareStatement(sql2);
			pre2.setString(1,book_status);
			pre2.setString(2,Del_flagEnum.EXIST.getCode());
			result2 = pre2.executeQuery();
			PageVO<BookStatusDTO> page = new PageVO<BookStatusDTO>();
			if(result2.next()) {
				page.setCurrentPage(currentPage);
				page.setListData(books);
				int count =Integer.valueOf(result2.getString("count"));
				int totalPage =count%ProjectConfig.PAGE_SIZE>0?count/ProjectConfig.PAGE_SIZE+1:count/ProjectConfig.PAGE_SIZE;  
				page.setNumber(result2.getInt("count"));
				page.setTotalPage(totalPage);
			}
			return page;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
				OracleConfig.release(con, pre2, result2);
		}
	}
	
	public ResultVO deleteBook(String book_id) {   //删除图书
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;
		ResultSet result = null;
		PreparedStatement pre2 = null;
		PreparedStatement pre3 = null;
		try {
			con = OracleConfig.getConnection();
			con.setAutoCommit(false);
			//更改book标识
			String sql = "update book set del_flag = ? where id = ?";
			pre1 = con.prepareStatement(sql);// 实例化预编译语句
			pre1.setString(1, Del_flagEnum.DELETE.getCode());
			pre1.setString(2,book_id);
			pre1.executeQuery();
			//查询书籍id对应的图书位id
			String sql2 = "select book_location_id from book where id = ?";
			pre2 = con.prepareStatement(sql2);// 实例化预编译语句
			pre2.setString(1,book_id);
			result = pre2.executeQuery();
			String book_location_id = null;
			if(result.next()) {
				book_location_id  = result.getString("book_location_id");
			}
			if(book_location_id==null) {  //表示该书没在图书位
				return ResultVOUtil.success();		
			}
			//更新图书位状态
			String sql3 = "update book_location set location_status = ? where id = ?";
			pre3 = con.prepareStatement(sql3);// 实例化预编译语句
			pre3.setString(1,Book_status_Enum.BOOK_OUT.getCode());
			pre3.setString(2,book_location_id);
			pre3.executeQuery();
			con.commit();
			return ResultVOUtil.success();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				System.err.println("删除图书失败，事务回滚");
				e1.printStackTrace();
			}
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
				OracleConfig.release(con, pre2, result);
				OracleConfig.release(con, pre3, result);
		}
	}
	public BookDetailDTO searchBookDetail() {
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;
		PreparedStatement pre2 = null;
		PreparedStatement pre3= null;
		ResultSet result = null;
		ResultSet result2 = null;
		ResultSet result3 = null;
		try {
			con = OracleConfig.getConnection();
			//查询借出入库数目
			StringBuilder sql1 = new StringBuilder();
			sql1.append("select count(case when book_status = ?  then ? end) book_in, ");
			sql1.append("count(case when book_status = ? then ? end) book_out ");
			sql1.append("from book where del_flag = ?");
			pre1 = con.prepareStatement(sql1.toString());// 实例化预编译语句
			pre1.setString(1, Book_status_Enum.BOOK_IN.getCode());
			pre1.setString(2, Book_status_Enum.BOOK_IN.getCode());
			pre1.setString(3,Book_status_Enum.BOOK_OUT.getCode());
			pre1.setString(4,Book_status_Enum.BOOK_OUT.getCode());
			pre1.setString(5, Del_flagEnum.EXIST.getCode());
			result = pre1.executeQuery();
			BookDetailDTO bookDetailDTO = new BookDetailDTO();
			if(result.next()) {
				bookDetailDTO.setBook_in(result.getInt("book_in"));
				bookDetailDTO.setBook_out(result.getInt("book_out"));
			}
			
			//得到所有klass_id
			List<String> klass_idList = new ArrayList<String>();
			String sql2 = "select id from book_klass";
			pre2 = con.prepareStatement(sql2);// 实例化预编译语句
			result2 = pre2.executeQuery();
			while(result2.next()) {
					klass_idList.add(result2.getString("id"));
			}
			//遍历所有类分组得到图书数量
			StringBuilder sql3 = new StringBuilder();
			String[] klassName = {"a_klass","b_class","c_class"};  //自定义伪列
			sql3.append("select ");
			for(int i = 0;i<klass_idList.size();i++) {
				sql3.append("count(case when book.book_klass_id = ? then ? end) "+klassName[i]+" ,"); //假如以后会有很多中类加入
			}
			sql3.append("count(1) total ");
			sql3.append("from book  left join  book_klass ");
			sql3.append("on book.book_klass_id = book_klass.id ");
			sql3.append("where book.del_flag = 0");
			System.out.println(sql3.toString());
			pre3 = con.prepareStatement(sql3.toString());// 实例化预编译语句
			int after =2;
			int before = 1;
			for(int i = 0;i<klass_idList.size();i++) {
				pre3.setString(before,klass_idList.get(i)); //假如以后会有很多中类加入
				pre3.setString(after, klass_idList.get(i)); //假如以后会有很多中类加入	
				after = after+2;
				before = before +2;
			} 
			result3= pre3.executeQuery();
			if(result3.next()) {
				bookDetailDTO.setA_klass(result3.getInt(klassName[0]));
				bookDetailDTO.setB_klass(result3.getInt(klassName[1]));
				bookDetailDTO.setTotal(result3.getInt("total"));   
			}
			System.out.println(bookDetailDTO.toString());
			return bookDetailDTO;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
				OracleConfig.release(con, pre2, result);
				OracleConfig.release(con, pre3, result);
		}
	}
	public ResultVO LendBook(String book_id,String book_status) { //借出书
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		PreparedStatement pre2 = null;
		PreparedStatement pre3 = null;
		ResultSet result = null;// 创建一个结果集对象
		try {
			con = OracleConfig.getConnection();
			String status = getBookStatus(book_id);
			con.setAutoCommit(false);
			if(status==null) {
				System.err.println("该书查找不到");
				return ResultVOUtil.error(ResultEnum.BOOK_GET_ERROR.getCode(), ResultEnum.BOOK_GET_ERROR.getMessage());
			}else {
				if(status.equals(book_status)) {
					return ResultVOUtil.error(ResultEnum.PARAMETER_REPEAT.getCode(), ResultEnum.PARAMETER_REPEAT.getMessage());
				}else {		
					Long lendTime = new Date().getTime();				
					
					// 查询该书的信息
					String sql2 = "select book_name,book_location_id from book where id = ? and del_flag = ?";
					pre2 = con.prepareStatement(sql2);
					pre2.setString(1, book_id);
					pre2.setString(2, Del_flagEnum.EXIST.getCode());
					result = pre2.executeQuery();		
					String book_location_id = null;
					if(result.next()) {
						//插入书本历史记录
						book_location_id = result.getString("book_location_id");
						book_historyDao.save(con, pre1, book_id, result.getString("book_name"), book_status, book_location_id,lendTime.toString());
					}
					System.out.println("借出的书原本的图书位id："+book_location_id);
					
					//更换该书本的的状态
					String sql  = "update book set book_status = ?,lend_time = ?,book_location_id = ? where id = ? and del_flag = ? ";
					pre1 = con.prepareStatement(sql);
					pre1.setString(1, book_status);		
					pre1.setString(2, lendTime.toString());
					pre1.setString(3, null);
					pre1.setString(4, book_id);
					pre1.setString(5, Del_flagEnum.EXIST.getCode());
					pre1.executeQuery();
					//4.让出图书位 
					String sql3 = "update book_location set location_status = ? where id = ?";
					pre3 = con.prepareStatement(sql3);
					pre3.setString(1, Book_status_Enum.BOOK_OUT.getCode());
					pre3.setString(2, book_location_id);
					pre3.executeQuery();	
					con.commit();
					return ResultVOUtil.success();		
				}	
			}	
		} catch (Exception e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				System.err.println("书的借出失败");
				e1.printStackTrace();
			}
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
				OracleConfig.release(con, pre2, result);
				OracleConfig.release(con, pre3, result);
		}
	}
	public ResultVO intoBook(String book_id,String book_status,String location_id,String klassName) { //还书 location_id通过service层同步的方法获得，以免冲突
		System.out.println("进入书库的图书位："+location_id);
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		PreparedStatement pre2 = null;
		PreparedStatement pre3 = null;
		PreparedStatement pre4 = null;
		PreparedStatement pre5 = null;
		ResultSet result = null;// 创建一个结果集对象
		try {
			con = OracleConfig.getConnection();
			con.setAutoCommit(false);//禁止自动提交
			String status = getBookStatus(book_id);
			if(status==null) {
				System.err.println("该书查找不到");
				return ResultVOUtil.error(ResultEnum.BOOK_GET_ERROR.getCode(), ResultEnum.BOOK_GET_ERROR.getMessage());
			}else {
				if(status.equals(book_status)) {   //参数重复
					return ResultVOUtil.error(ResultEnum.PARAMETER_REPEAT.getCode(), ResultEnum.PARAMETER_REPEAT.getMessage());
				}else {		
					Long endTime = new Date().getTime();//入库时间
					String book_name = null;
					String lend_time = null;  //借书时间		
					//1.查询并记录历史数据
					String sql1 = "select book_name,lend_time from book where id = ? and del_flag = ?";
					pre1 = con.prepareStatement(sql1);
					pre1.setString(1, book_id);
					pre1.setString(2, Del_flagEnum.EXIST.getCode());
					result = pre1.executeQuery();
					if(result.next()) {
						//再插入书本历史记录
						lend_time = result.getString("lend_time");
						book_name = result.getString("book_name");
						book_historyDao.save(con, pre5, book_id, book_name, book_status, location_id,endTime.toString());
					}
					
					
					//2.更换该书本的的状态和占图书位
					String sql2  = "update book set book_status = ?,lend_time = ?,book_location_id=? where id = ? and del_flag = ?";
					pre2 = con.prepareStatement(sql2);
					pre2.setString(1, book_status);  //更改状态		
					pre2.setString(2, null);  //借出时间清空
					pre2.setString(3, location_id);
					pre2.setString(4, book_id);
					pre2.setString(5, Del_flagEnum.EXIST.getCode());
					pre2.executeQuery();
				

				
					//3. 重新入库占图书位
					String sql3 = "update book_location set location_status = ? where  id = ? ";
					pre3 = con.prepareStatement(sql3);
					pre3.setString(1, Book_status_Enum.BOOK_IN.getCode());
					pre3.setString(2,location_id);
					pre3.executeQuery();
					
					//4 .财务方面:
				Double allMoney = TimeForMoney.getAllmoney2(endTime, new Long(lend_time), klassName);
						
				//5.消费明细入库
				String sql4 = "insert into book_charge (id,book_name,location_number,money,book_out_time,book_in_time,book_id) values(?,?,?,?,?,?,?)";
				pre4 = con.prepareStatement(sql4);
				pre4.setString(1, KeyUtil.genUniqueKey());
				pre4.setString(2, book_name);
				pre4.setString(3, location_id);
				pre4.setDouble(4, allMoney);
				pre4.setString(5,lend_time);
				pre4.setString(6,endTime.toString());
				pre4.setString(7, book_id);
				pre4.executeQuery();	
				
				
				con.commit();
				IntoBookDTO intoBook = new IntoBookDTO();
				intoBook.setLocaltion_id(location_id);
				intoBook.setMoney(allMoney);
				return ResultVOUtil.success(intoBook);		
				}	
			}	
		} catch (Exception e) {
			e.printStackTrace();
			try {
				con.rollback();
				System.err.println("书的入库失败");
			} catch (SQLException e1) {
				System.err.println("书的入库失败");
				e1.printStackTrace();
			}
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
				OracleConfig.release(con, pre2, result);
				OracleConfig.release(con, pre3, result);
				OracleConfig.release(con, pre4, result);
		}
	}
	
	public String getBookStatus(String book_id) {
		Connection con = null;
		PreparedStatement pre1 = null;
		ResultSet result = null;
		try {
			con = OracleConfig.getConnection();
			//根据id查询该书状态。
			String sql  = "select book_status from book where id = ? and del_flag = ?";
			pre1 = con.prepareStatement(sql);
			pre1.setString(1, book_id);
			pre1.setString(2, Del_flagEnum.EXIST.getCode());
			result = pre1.executeQuery();
			if(result.next()) {
				return result.getString("book_status");
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
		}
	}
	
	
	
	public ResultVO getPageForAll_book(String currentPage) {   //分页获取全部图书
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;
		try {
			con = OracleConfig.getConnection();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("select * from");
			stringBuilder.append("(select a1.*,rownum rn from "); 
			stringBuilder.append(" (select id,book_name,book_status,book_location_id,book_img,");
			stringBuilder.append("  (select name from book_klass where book_klass.id = b.book_klass_id) klass_name");
			stringBuilder.append("   from book b where del_flag = ? order by b.id desc) a1) where rn between ? and ?");
			pre1 = con.prepareStatement(stringBuilder.toString());// 实例化预编译语句
			pre1.setString(1, Del_flagEnum.EXIST.getCode());
			int startLocated = ((Integer.valueOf(currentPage)-1)*ProjectConfig.PAGE_SIZE)+1;
			int endLocated =Integer.valueOf(currentPage)* ProjectConfig.PAGE_SIZE;
			pre1.setString(2,String.valueOf(startLocated));
			pre1.setString(3,String.valueOf(endLocated));
			System.out.println("开始："+startLocated+"，结束："+endLocated);
			result = pre1.executeQuery();  
			List<BookStatusDTO> books = new ArrayList<BookStatusDTO>();
			
			while(result.next()) {
				BookStatusDTO bookStatusDTO = new BookStatusDTO();
				bookStatusDTO.setBook_name(result.getString("book_name"));
				bookStatusDTO.setBook_status(result.getString("book_status"));
				bookStatusDTO.setId(result.getString("id"));
				bookStatusDTO.setKlass_name(result.getString("klass_name"));
				bookStatusDTO.setRownum(result.getString("rn"));
				bookStatusDTO.setBook_img(result.getString("book_img"));
				bookStatusDTO.setBook_location_id(result.getString("book_location_id"));
				books.add(bookStatusDTO);
			}  
			return   ResultVOUtil.success(books);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
		}
	}
	
	
	public ResultVO searchBookName(String book_name) {   //查询书名
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;
		try {
			con = OracleConfig.getConnection();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("select book_name from");
			stringBuilder.append(" book where book_name like ?");
			stringBuilder.append("and del_flag = ?");
			pre1 = con.prepareStatement(stringBuilder.toString());// 实例化预编译语句
			pre1.setString(1,"%"+book_name+"%");
			pre1.setString(2,Del_flagEnum.EXIST.getCode());
			result = pre1.executeQuery();  
			List<String> books = new ArrayList<String>();	
			while(result.next()) {
				books.add(result.getString("book_name"));
			}  
			return   ResultVOUtil.success(books);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
		}
	}
	
	
	
	public ResultVO searchBookForLike(String book_name) {   //模糊查询图书详情
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;
		try {
			con = OracleConfig.getConnection();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("select id,book_name,book_status,book_location_id,book_img, ");
			stringBuilder.append("(select name from book_klass where book_klass.id = b.book_klass_id) klass_name"); 
			stringBuilder.append("  from book b where del_flag = ? and book_name like ? order by b.id desc");
			pre1 = con.prepareStatement(stringBuilder.toString());// 实例化预编译语句
			pre1.setString(1, Del_flagEnum.EXIST.getCode());
			pre1.setString(2,"%"+book_name+"%");
			result = pre1.executeQuery();  
			List<BookStatusDTO> books = new ArrayList<BookStatusDTO>();
			while(result.next()) {
				BookStatusDTO bookStatusDTO = new BookStatusDTO();
				bookStatusDTO.setBook_name(result.getString("book_name"));
				bookStatusDTO.setBook_status(result.getString("book_status"));
				bookStatusDTO.setId(result.getString("id"));
				bookStatusDTO.setKlass_name(result.getString("klass_name"));
				bookStatusDTO.setBook_img(result.getString("book_img"));
				bookStatusDTO.setBook_location_id(result.getString("book_location_id"));
				books.add(bookStatusDTO);
			}  
			return   ResultVOUtil.success(books);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
		}
	}
	
	
	
}
