package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import config.OracleConfig;
import config.ProjectConfig;
import dto.Book_chargeDTO;
import vo.PageVO;
public class Book_chargeDao {
	public PageVO<Book_chargeDTO> getBookHistory(String currentPage) {   //分页得到图书进出历史记录
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;
		PreparedStatement pre2 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result2 = null;
		try {
			con = OracleConfig.getConnection();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("select * from  ");
			stringBuilder.append("  (select a1.*,rownum rn from ");
			stringBuilder.append("(select book_name,location_number,money,book_out_time,book_in_time from book_charge b order by b.book_in_time desc) a1 ");
			stringBuilder.append(" ) a2 ");
			stringBuilder.append("where a2.rn between ? and ? ");
			pre1 = con.prepareStatement(stringBuilder.toString());// 实例化预编译语句
			int startLocated = ((Integer.valueOf(currentPage)-1)*ProjectConfig.PAGE_SIZE)+1;
			int endLocated =Integer.valueOf(currentPage)* ProjectConfig.PAGE_SIZE;
			pre1.setString(1,String.valueOf(startLocated));
			pre1.setString(2,String.valueOf(endLocated));
			System.out.println("开始："+startLocated+"，结束："+endLocated);
			result = pre1.executeQuery();  
			PageVO<Book_chargeDTO> page = new PageVO<Book_chargeDTO>();
			List<Book_chargeDTO> books = new ArrayList<Book_chargeDTO>();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
			while(result.next()) {
				Book_chargeDTO book_historyDTO = new Book_chargeDTO();
				book_historyDTO.setBook_name(result.getString("book_name"));
				book_historyDTO.setLocation_number(result.getString("location_number"));
				book_historyDTO.setMoney(result.getDouble("money"));
				book_historyDTO.setBook_out_time(sdf.format(new Date(new Long(result.getString("book_out_time")))));
				book_historyDTO.setBook_in_time(sdf.format(new Date(new Long(result.getString("book_in_time")))));
				book_historyDTO.setRownum(result.getString("rn"));  
				books.add(book_historyDTO);
			}
			String sql2 = "select count(*) total from book_charge";
			pre2 = con.prepareStatement(sql2);
			result2 = pre2.executeQuery();  
			if(result2.next()) {
				int total =result2.getInt("total");		
				int totalPage =total%ProjectConfig.PAGE_SIZE>0?total/ProjectConfig.PAGE_SIZE+1:total/ProjectConfig.PAGE_SIZE;  
				page.setNumber(total);
				page.setTotalPage(totalPage);
			}			
			page.setListData(books);
			page.setCurrentPage(currentPage);
			return page;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
				OracleConfig.release(con, pre2, result2);
		}
	}
}
