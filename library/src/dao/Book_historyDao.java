package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import config.OracleConfig;
import config.ProjectConfig;
import dto.BookStatusDTO;
import dto.Book_historyDTO;
import enums.Del_flagEnum;
import util.KeyUtil;
import util.ResultVOUtil;
import vo.PageVO;
import vo.ResultVO;
public class Book_historyDao {
	public  ResultVO save(Connection con,PreparedStatement pre,String book_id,String book_name,String book_status,String location_id,String lend_time) throws Exception{   //保存进出库记录
			System.out.println(book_id+"  :   "+book_name+" ： "+book_status+"  :  "+location_id+" :  "+lend_time);
			String sql = "insert into book_history (id,book_name,book_status,location_id,create_time,book_id) values (?,?,?,?,?,?)";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			pre.setString(1, KeyUtil.genUniqueKey());
			pre.setString(2, book_name);
			pre.setString(3, book_status);
			pre.setString(4, location_id);
			pre.setString(5,lend_time);
			pre.setString(6, book_id);
			pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			return ResultVOUtil.success();
//finally {   这里就不关闭了，别人传进来别人关闭
//			OracleConfig.release(con, pre, null);
//		}
	}
	
	
	public PageVO<Book_historyDTO> getBookHistory(String currentPage) {   //分页得到图书进出历史记录
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre1 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;
		PreparedStatement pre2 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result2 = null;
		try {
			con = OracleConfig.getConnection();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("select * from  ");
			stringBuilder.append(" (select a1.*,rownum rn from ");
			stringBuilder.append("(select book_name,book_status,location_id,create_time from book_history b order by b.create_time desc) a1 ");
			stringBuilder.append(" ) a2 ");
			stringBuilder.append("where a2.rn between ? and ? ");
			pre1 = con.prepareStatement(stringBuilder.toString());// 实例化预编译语句
			int startLocated = ((Integer.valueOf(currentPage)-1)*ProjectConfig.PAGE_SIZE)+1;
			int endLocated =Integer.valueOf(currentPage)* ProjectConfig.PAGE_SIZE;
			pre1.setString(1,String.valueOf(startLocated));
			pre1.setString(2,String.valueOf(endLocated));
			System.out.println("开始："+startLocated+"，结束："+endLocated);
			result = pre1.executeQuery();  
			PageVO<Book_historyDTO> page = new PageVO<Book_historyDTO>();
			List<Book_historyDTO> books = new ArrayList<Book_historyDTO>();
			while(result.next()) {
				Book_historyDTO book_historyDTO = new Book_historyDTO();
				book_historyDTO.setBook_name(result.getString("book_name"));
				book_historyDTO.setBook_status(result.getString("book_status"));
				book_historyDTO.setCreate_time(result.getString("create_time"));
				book_historyDTO.setLocation_id(result.getString("location_id"));
				book_historyDTO.setRownum(result.getString("rn"));
				books.add(book_historyDTO);
			}
			String sql2 = "select count(*) total from book_history";
			pre2 = con.prepareStatement(sql2);
			result2 = pre2.executeQuery();  
			if(result2.next()) {
				int total =result2.getInt("total");		
				int totalPage =total%ProjectConfig.PAGE_SIZE>0?total/ProjectConfig.PAGE_SIZE+1:total/ProjectConfig.PAGE_SIZE;  
				page.setNumber(total);
				page.setTotalPage(totalPage);
			}			
			page.setListData(books);
			page.setCurrentPage(currentPage);
			return page;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
				OracleConfig.release(con, pre1, result);
				OracleConfig.release(con, pre2, result2);
		}
	}
	
}
