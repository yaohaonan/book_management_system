package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import config.OracleConfig;
import entity.Book_klass;
public class Book_klassDao {
	public  List<Book_klass> findAllForBook_klass() {
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;// 创建一个结果集对象
		try {
			Class.forName(OracleConfig.DRIVER);// 加载Oracle驱动程序
			con = DriverManager.getConnection(OracleConfig.JDBCURL, OracleConfig.USERNAME, OracleConfig.PASSWORD);// 获取连接
			String sql = "select id,name from book_klass";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			List<Book_klass> book_klasses = new ArrayList<>();
			while (result.next()) {
				Book_klass book_klass = new Book_klass();
				book_klass.setId(result.getString("id"));
				book_klass.setName(result.getString("name"));
				book_klasses.add(book_klass);
			}	
			return book_klasses;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleConfig.release(con, pre, result);
		}
		return null;
	}
	
	public  Book_klass findByName(String klassName) {   //根据类名查询
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;// 创建一个结果集对象
		try {
			Class.forName(OracleConfig.DRIVER);// 加载Oracle驱动程序
			con = DriverManager.getConnection(OracleConfig.JDBCURL, OracleConfig.USERNAME, OracleConfig.PASSWORD);// 获取连接
			String sql = "select id,name from book_klass where name = ?";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			pre.setString(1, klassName);
			result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			if(result.next()) { 
				Book_klass book_klass = new Book_klass();
				book_klass.setId(result.getString("id"));
				book_klass.setName(result.getString("name"));
				return book_klass;
			}	
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleConfig.release(con, pre, result);
		}
		return null;
	}
	
	public  String findByBook_id(String book_id) {   //根据类名查询
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;// 创建一个结果集对象
		try {
			con = OracleConfig.getConnection();
			String sql = "select  bk.name klassname from book b right join book_klass bk  on b.book_klass_id = bk.id where b.id = ?";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			pre.setString(1, book_id);
			result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			if(result.next()) { 
				return result.getString("klassname");
			}	
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleConfig.release(con, pre, result);
		}
		return null;
	}
	
}
