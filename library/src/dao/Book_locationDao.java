package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import config.OracleConfig;
import config.ProjectConfig;
import dto.Book_locationDTO;
import entity.Book_location;
import enums.Book_status_Enum;
import enums.Del_flagEnum;
import enums.ResultEnum;
import util.KeyUtil;
import util.ResultVOUtil;
import vo.LocationVO;
import vo.PageVO;
import vo.ResultVO;
public class Book_locationDao {
	public List<String> getFreeLocation(String book_klass_id) {   //查询空位
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;// 创建一个结果集对象
		try {		
			con = OracleConfig.getConnection();
			String sql = "select id from book_location where book_klass_id = ? and location_status = ? and del_flag = ?";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			pre.setString(1, book_klass_id);
			pre.setString(2, Book_status_Enum.BOOK_OUT.getCode());
			pre.setString(3, Del_flagEnum.EXIST.getCode());
			result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			List<String> location_number_list = new ArrayList<String>();
				while(result.next()) {
					 location_number_list.add(result.getString("id"));			
				}	
				 return location_number_list;	
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
				OracleConfig.release(con, pre, result);
		}
		return null;
	}
	 //查询该类所有的图书位(分页)
	public PageVO getKlassForLocation(String book_klass_id,String currentPage) {  
		System.out.println(book_klass_id+"：："+currentPage);
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
//		PreparedStatement pre2 = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;// 创建一个结果集对象
//		ResultSet result2 = null;  
		try {
			con = OracleConfig.getConnection();
			//查询分页数据
			String sql = "select  a1.* from  (select id,book_klass_id,location_status,rownum rn from book_location where del_flag = ? and book_klass_id = ?) a1 where rn between ? and ?";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			pre.setString(1, Del_flagEnum.EXIST.getCode());
			pre.setString(2, book_klass_id);
			int startLocated = ((Integer.valueOf(currentPage)-1)*ProjectConfig.PAGE_SIZE)+1;
			int endLocated = Integer.valueOf(currentPage) * ProjectConfig.PAGE_SIZE;
			pre.setString(3,String.valueOf(startLocated));
			pre.setString(4,String.valueOf(endLocated));
			System.out.println("开始："+startLocated+"，结束："+endLocated);
			List<Book_locationDTO> book_locationDTOs = new ArrayList<Book_locationDTO>();
			PageVO page = new PageVO<>();
			result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			while(result.next()) {
				Book_locationDTO book_locationDTO = new Book_locationDTO();
				book_locationDTO.setId(result.getString("id"));
				book_locationDTO.setBook_klass_id(result.getString("book_klass_id"));
				book_locationDTO.setLocation_status(result.getString("location_status"));
				book_locationDTO.setRownum(result.getString("rn"));
				book_locationDTOs.add(book_locationDTO);
			}	
			//查询总页数
			String sql2 = "select count(*) count from book_location where  book_klass_id = ? and del_flag = ?";
			pre = con.prepareStatement(sql2);// 实例化预编译语句
			pre.setString(1, book_klass_id);
			pre.setString(2, Del_flagEnum.EXIST.getCode());
			result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数	
				if(result.next()) {
					page.setCurrentPage(currentPage);
					page.setListData(book_locationDTOs);
					int count =Integer.valueOf(result.getString("count"));
					int totalPage =count%ProjectConfig.PAGE_SIZE>0?count/ProjectConfig.PAGE_SIZE+1:count/ProjectConfig.PAGE_SIZE;  
					page.setNumber(count);
					page.setTotalPage(totalPage);
					return page;
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
				OracleConfig.release(con, pre, result);
		}
		return null;
	}
	//删除图书位
	public ResultVO deleteBook_location(String location_id) {   //查询空位
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		PreparedStatement pre2 = null;
		ResultSet result = null;// 创建一个结果集对象
		try {
			Class.forName(OracleConfig.DRIVER);// 加载Oracle驱动程序
			con = DriverManager.getConnection(OracleConfig.JDBCURL, OracleConfig.USERNAME, OracleConfig.PASSWORD);// 获取连接
			
			//查询图书位状态
			String sql2 = "select location_status from book_location where id = ? ";
			pre = con.prepareStatement(sql2);// 实例化预编译语句
			pre.setString(1, location_id);
			result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			if(result.next()) {
				if(Book_status_Enum.BOOK_IN.getCode().equals(result.getString("location_status"))) {
					return  ResultVOUtil.error(ResultEnum.LOCATION_HAVEBOOK.getCode(), ResultEnum.LOCATION_HAVEBOOK.getMessage());
				}else {
					//实行软删除
					String sql = "update book_location  set del_flag = ? where id = ? ";
					pre2 = con.prepareStatement(sql);// 实例化预编译语句
					pre2.setString(1, Del_flagEnum.DELETE.getCode());
					pre2.setString(2, location_id);
					pre2.executeQuery();
					 return ResultVOUtil.success();
				}		
			}else {
				return ResultVOUtil.error(ResultEnum.LOCATION_ERROR.getCode(), ResultEnum.LOCATION_ERROR.getMessage());
			}
			
		} catch (Exception e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				// 逐一将上面的几个对象关闭，因为不关闭的话会影响性能、并且占用资源
				// 注意关闭的顺序，最后使用的最先关闭
				if (result != null)
					result.close();
				if (pre != null)
					pre.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	//删除图书位
	public ResultVO getNumberForLocation(String klass_id) {   //查询图书位详情。
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		PreparedStatement pre2 = null;
		ResultSet result = null;// 创建一个结果集对象
		try {
			Class.forName(OracleConfig.DRIVER);// 加载Oracle驱动程序
			con = DriverManager.getConnection(OracleConfig.JDBCURL, OracleConfig.USERNAME, OracleConfig.PASSWORD);// 获取连接
			
			//查询图书位状态
			StringBuilder sqlStringBuilder = new StringBuilder();
			sqlStringBuilder.append("select count(case when location_status = ? then ? end) remain, ");
			sqlStringBuilder.append("count(case when location_status = ? then ? end) occupy, ");
			sqlStringBuilder.append("count(*) total ");
			sqlStringBuilder.append(" from book_location ");
			sqlStringBuilder.append("where book_klass_id = ?  ");
			sqlStringBuilder.append("and del_flag = ?");
			pre = con.prepareStatement(sqlStringBuilder.toString());// 实例化预编译语句
			pre.setString(1, Book_status_Enum.BOOK_OUT.getCode());
			pre.setString(2, Book_status_Enum.BOOK_OUT.getCode());
			pre.setString(3, Book_status_Enum.BOOK_IN.getCode());
			pre.setString(4, Book_status_Enum.BOOK_IN.getCode());
			pre.setString(5, klass_id);
			pre.setString(6, Del_flagEnum.EXIST.getCode());
			result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			if (result.next()) {
						LocationVO locationVO = new LocationVO();
						locationVO.setOccupy(result.getInt("occupy"));
						locationVO.setRemain(result.getInt("remain"));
						locationVO.setTotal(result.getInt("total"));
						return ResultVOUtil.success(locationVO);
				} else {
						return ResultVOUtil.error(ResultEnum.LOCATION_COUNT_ERROR.getCode(), ResultEnum.LOCATION_COUNT_ERROR.getMessage());	
				}	
		} catch (Exception e) {
				e.printStackTrace();
		} finally {
			try {
				// 逐一将上面的几个对象关闭，因为不关闭的话会影响性能、并且占用资源
				// 注意关闭的顺序，最后使用的最先关闭
				if (result != null)
					result.close();
				if (pre != null)
					pre.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ResultVOUtil.error(ResultEnum.LOCATION_COUNT_ERROR.getCode(), ResultEnum.LOCATION_COUNT_ERROR.getMessage());	
	}
	
	public ResultVO  addBook_location(String klass_id,String number) {   //查询空位
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		try {
			Class.forName(OracleConfig.DRIVER);// 加载Oracle驱动程序
			con = DriverManager.getConnection(OracleConfig.JDBCURL, OracleConfig.USERNAME, OracleConfig.PASSWORD);// 获取连接
			String sql = "insert into book_location (id,book_klass_id,location_status,del_flag) values (?,?,?,?)";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			for(int i = 0;i<Integer.valueOf(number);i++) {
				String id = KeyUtil.genUniqueKey();
				System.out.println("已增加该图书位的id为："+id);
				pre.setString(1, id);
				pre.setString(2, klass_id);
				pre.setString(3, Book_status_Enum.BOOK_OUT.getCode());
				pre.setString(4, Del_flagEnum.EXIST.getCode());
				pre.executeQuery();
			}		
			return ResultVOUtil.success();
		} catch (Exception e) {
			e.printStackTrace();
			return ResultVOUtil.error(ResultEnum.UNKNOW_ERROR.getCode(), ResultEnum.UNKNOW_ERROR.getMessage());
		} finally {
			try {
				// 逐一将上面的几个对象关闭，因为不关闭的话会影响性能、并且占用资源
				// 注意关闭的顺序，最后使用的最先关闭
				if (pre != null)
					pre.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void  alterKlass_id(String book_klass_id,String location_id) {   //更改类
		System.out.println("klassId :"+book_klass_id+"   location_id:"+location_id);
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		try {		
			con = OracleConfig.getConnection();
			String sql = "update book_location set book_klass_id = ? where id = ? ";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			pre.setString(1, book_klass_id);
			pre.setString(2, location_id);
		    pre.executeQuery();// 执行查询，注意括号中不需要再加参数
		    String sql2 = "update book set book_klass_id = ? where book_location_id = ?";
			pre = con.prepareStatement(sql2);// 实例化预编译语句
			pre.setString(1, book_klass_id);
			pre.setString(2, location_id);
		    pre.executeQuery();// 执行查询，注意括号中不需要再加参数
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
				OracleConfig.release(con, pre, null);
		}
	}
	
	public Book_location  getBook_location(String location_id) {   //更改类
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;
		try {		
			con = OracleConfig.getConnection();
			String sql = "select id,book_klass_id,location_status from book_location where id = ? and del_flag = ?";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			pre.setString(1, location_id);
			pre.setString(2, Del_flagEnum.EXIST.getCode());
			result =   pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			if(result.next()) {
				Book_location book_location = new Book_location();
				book_location.setId(result.getString("id"));
				book_location.setBook_klass_id(result.getString("book_klass_id"));
				book_location.setLocation_status(result.getString("location_status"));
				return book_location;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
				OracleConfig.release(con, pre, result);
		}
		return null;
	}
}
