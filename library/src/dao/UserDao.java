package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import config.OracleConfig;
import entity.User_;

public class UserDao {
	public  User_ findByUsernameAndPassword(String username,String password) {
		Connection con = null;// 创建一个数据库连接
		PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		ResultSet result = null;// 创建一个结果集对象
		try {
			Class.forName(OracleConfig.DRIVER);// 加载Oracle驱动程序
			con = DriverManager.getConnection(OracleConfig.JDBCURL, OracleConfig.USERNAME, OracleConfig.PASSWORD);// 获取连接
			String sql = "select id,username,create_time from user_ where username= ? and password = ?";
			pre = con.prepareStatement(sql);// 实例化预编译语句
			pre.setString(1, username);// 设置参数，前面的1表示参数的索引，而不是表中列名的索引
			pre.setString(2, password);
			result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数
			if(result.next()) {
				System.out.println(result.getString("id")+":登陆成功");		
				User_  user_ = new User_();
				user_.setId(result.getString("id"));
				user_.setUsername(result.getString("username"));
				user_.setCreate_time(result.getDate("create_time"));
				return user_;
			}else {
				System.out.println("登陆失败");
				return null;
			}			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// 逐一将上面的几个对象关闭，因为不关闭的话会影响性能、并且占用资源
				// 注意关闭的顺序，最后使用的最先关闭
				if (result != null)
					result.close();
				if (pre != null)
					pre.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
}
