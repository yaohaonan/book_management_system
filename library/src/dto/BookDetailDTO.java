package dto;

public class BookDetailDTO {
	private int book_in;  //入库数量
	private int book_out; //出库数量
	private int a_klass;  //a类书
	private int b_klass;  //b类书
	private int total;  //总数
	
	@Override
	public String toString() {
		return "BookDetailDTO [book_in=" + book_in + ", book_out=" + book_out + ", a_klass=" + a_klass + ", b_klass="
				+ b_klass + ", total=" + total + "]";
	}
	public int getBook_in() {
		return book_in;
	}
	public void setBook_in(int book_in) {
		this.book_in = book_in;
	}
	public int getBook_out() {
		return book_out;
	}
	public void setBook_out(int book_out) {
		this.book_out = book_out;
	}
	public int getA_klass() {
		return a_klass;
	}
	public void setA_klass(int a_klass) {
		this.a_klass = a_klass;
	}
	public int getB_klass() {
		return b_klass;
	}
	public void setB_klass(int b_klass) {
		this.b_klass = b_klass;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
}
