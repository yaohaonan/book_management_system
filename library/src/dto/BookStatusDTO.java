package dto;

public class BookStatusDTO {
	private String id;
	private String book_name; // 书本名称
	private String klass_name; //类名
	private String book_status; // 书本状态(借出或者在书库中)
	private String book_img;
	private String book_location_id;  //图书位id
	private String rownum;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getBook_status() {
		return book_status;
	}
	public void setBook_status(String book_status) {
		this.book_status = book_status;
	}
	public String getRownum() {
		return rownum;
	}
	public void setRownum(String rownum) {
		this.rownum = rownum;
	}
	public String getKlass_name() {
		return klass_name;
	}
	public void setKlass_name(String klass_name) {
		this.klass_name = klass_name;
	}
	public String getBook_img() {
		return book_img;
	}
	public void setBook_img(String book_img) {
		this.book_img = book_img;
	}
	public String getBook_location_id() {
		return book_location_id;
	}
	public void setBook_location_id(String book_location_id) {
		this.book_location_id = book_location_id;
	}
	
	
}
