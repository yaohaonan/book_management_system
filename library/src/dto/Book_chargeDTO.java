package dto;

import entity.Book_charge;

public class Book_chargeDTO extends Book_charge {
	private String rownum;

	public String getRownum() {
		return rownum;
	}

	public void setRownum(String rownum) {
		this.rownum = rownum;
	}
}
