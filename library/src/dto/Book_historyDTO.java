package dto;

import entity.Book_history;

public class Book_historyDTO extends Book_history {
	private String rownum;

	public String getRownum() {
		return rownum;
	}

	public void setRownum(String rownum) {
		this.rownum = rownum;
	}

}
