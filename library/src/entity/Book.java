package entity;
//图书表
public class Book {
	private String id;
	private String book_name; // 书本名称
	private String book_status; // 书本状态(借出或者在书库中)
    private String book_location_id;  //书本入库的图书位
	private String book_klass_id; // 逻辑外键(指向book_klass)图书类
	private String lend_time; //借出去的时间
	private String book_img;//图书图片
	@Override
	public String toString() {
		return "Book [id=" + id + ", book_name=" + book_name + ", book_status=" + book_status + ", book_location_id="
				+ book_location_id + ", book_klass_id=" + book_klass_id + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBook_name() {
		return book_name;
	}

	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}

	public String getBook_status() {
		return book_status;
	}

	public void setBook_status(String book_status) {
		this.book_status = book_status;
	}

	public String getBook_klass_id() {
		return book_klass_id;
	}

	public void setBook_klass_id(String book_klass_id) {
		this.book_klass_id = book_klass_id;
	}

	public String getBook_location_id() {
		return book_location_id;
	}

	public void setBook_location_id(String book_location_id) {
		this.book_location_id = book_location_id;
	}

	public String getLend_time() {
		return lend_time;
	}

	public void setLend_time(String lend_time) {
		this.lend_time = lend_time;
	}

	public String getBook_img() {
		return book_img;
	}

	public void setBook_img(String book_img) {
		this.book_img = book_img;
	}

	
}
