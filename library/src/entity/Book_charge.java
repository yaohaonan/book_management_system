package entity;
//记录收费明细表
public class Book_charge {
	private String id;  
	private String book_name; //书名
	private String location_number; // 图书位的位置
	private double money; // 金额
	private String book_out_time; // 出库时间
	private String book_in_time; // 入库时间
	private String book_id; //图书id
	
	public String getBook_id() {
		return book_id;
	}
	public void setBook_id(String book_id) {
		this.book_id = book_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getLocation_number() {
		return location_number;
	}
	public void setLocation_number(String location_number) {
		this.location_number = location_number;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public String getBook_out_time() {
		return book_out_time;
	}
	public void setBook_out_time(String book_out_time) {
		this.book_out_time = book_out_time;
	}
	public String getBook_in_time() {
		return book_in_time;
	}
	public void setBook_in_time(String book_in_time) {
		this.book_in_time = book_in_time;
	}


}
