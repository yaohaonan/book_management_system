package entity;
//图书类 表
public class Book_klass {
	private String id;
	private String name;  //类名
//	private Integer location_size;  //图书类的图书位大小
	
	@Override
	public String toString() {
		return "Book_klass [id=" + id + ", name=" + name + "]";
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
