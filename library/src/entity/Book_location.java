package entity;

public class Book_location {
	private String id;
	private String book_klass_id; // 逻辑外键(指向图书类的id)
	private String location_status; // 图书位的状态(即是否已存了书本)
	private String del_flag; // 图书位的删除标志

	
	@Override
	public String toString() {
		return "Book_location [id=" + id + ", book_klass_id=" + book_klass_id + ", location_status=" + location_status
				+ ", del_flag=" + del_flag + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBook_klass_id() {
		return book_klass_id;
	}

	public void setBook_klass_id(String book_klass_id) {
		this.book_klass_id = book_klass_id;
	}

	public String getLocation_status() {
		return location_status;
	}

	public void setLocation_status(String location_status) {
		this.location_status = location_status;
	}

	public String getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(String del_flag) {
		this.del_flag = del_flag;
	}



}
