package entity;

import java.util.Date;
// �û���
public class User_ {
	private String id;
	private String username;
	private String password;
	private Date create_time;
	
	@Override
	public String toString() {
		return "User_ [id=" + id + ", username=" + username + ", password=" + password + ", create_time=" + create_time
				+ "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

}
