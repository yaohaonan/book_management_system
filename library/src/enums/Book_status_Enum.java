package enums;

public enum Book_status_Enum {
	  BOOK_OUT("0", "出库状态(或者图书位空闲状态)[也就是借出]"),
	  BOOK_IN("1","入库状态(或者图书位已占用状态)[也就是入库]"),
	    ;
	private String code;
    private String message;
    Book_status_Enum(String code, String message) {
        this.code = code;
        this.message = message;
    }
	public String getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
    
}
