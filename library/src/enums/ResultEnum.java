package enums;

public enum ResultEnum {
	    SUCCESS(0, "成功"),
	    LOGIN_ERROR(401,"登陆失败"),
	    KLASS_NOTFOUT(402,"书类未找到"),
	    LOCATION_NOTFOUNT(403,"没有多余的图书位可供存放了"),
	    SAVE_BOOK_FAIL(404,"书籍入库失败"),
	    BOOK_NAME_NULL(405,"书籍名为空"),
	    LOCATION_ERROR(406,"图书位错误"),
	    LOCATION_HAVEBOOK(407,"删除图书位失败,该图书位现在有书籍占用"),
	    LOCATION_COUNT_ERROR(408,"查询该类的图书位计数失败"),
	    UNKNOW_ERROR(409,"未知错误"),
	    PARAMETER_NULL(410,"参数为空"),
	    PARAMETER_ERROR(411,"参数有误"),
	    PARAMETER_REPEAT(412,"参数重复"),
	    BOOK_GET_ERROR(413,"获取书籍失败"),
	    BOOK_DETAIL_ERROR(414,"得到书籍数目分析出错"),
	    BOOK_HISTORY_SAVE_ERROR(415,"书籍进入库历史保存失败"),
	    BOOK_IMG_SIZE(416,"上传图片大小不符合系统要求【大于0KB小于2M】"),
	    BOOK_IMG_TYPE(417,"上传图片的类型不正确【目前仅接收jpg和png和jpeg】"),
	    ;
	    private Integer code;
	    private String message;
	    ResultEnum(Integer code, String message) {
	        this.code = code;
	        this.message = message;
	    }
		public Integer getCode() {
			return code;
		}
		public String getMessage() {
			return message;
		}
	    
}
