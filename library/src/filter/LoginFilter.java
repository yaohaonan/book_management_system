package filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import config.ProjectConfig;
import entity.User_;

/**
 * Servlet Filter implementation class LoginFilter
 */
//一个简单的过滤器
@WebFilter(  
		urlPatterns = { 
				"/ajax_getBook_charge", 
				"/Book_LocationServlet", 
				"/ImgUrlController", 
				"/JspServlet", 
				"/BookServlet", 
				"/ajax_reloadHistory"
		})
public class LoginFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		System.out.println("销毁过滤器");
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("过滤器启动");
		HttpServletRequest req = (HttpServletRequest) request;
		User_  user = (User_)req.getSession().getAttribute("user");
		if(user==null) {
			request.setAttribute("salt", ProjectConfig.SALT);  //登陆盐值
			request.getRequestDispatcher("/WEB-INF/page/login.jsp").forward(request, response);
		}else {
			chain.doFilter(request, response);
		}
		// pass the request along the filter chain
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("初始化过滤器");
	}

}
