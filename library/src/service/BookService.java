package service;

import java.util.List;

import dto.BookDTO;
import entity.Book;
import entity.Book_klass;
import vo.PageVO;
import vo.ResultVO;

public interface BookService {
	List<Book_klass> getBook_klass();   //得到书类
	ResultVO saveBook(String bookName,String klassName,String fileName);   //保存图书
	ResultVO getFreeLocation(String klassName);  //得到空余的位置
	PageVO getPageForBook_Location(String klass_id,String currentPage);   //分页得到图书位
	Book_klass findByName(String name);   //根据类名得到类
	ResultVO deleteLocation(String location_id);   //根据图书位id删除 图书位
	ResultVO getNumberForLocation(String klass_id);  //查看图书位计数
	ResultVO addBook_location(String klassName,String number);  //增加指定类的图书位
	ResultVO alterKlass_id(String klassName,String location_id);  //修改图书类的类
	ResultVO getPageForBook(String book_status,String currentPage); //分页得到图书
	ResultVO deleteBook(String book_id);  //删除图书
	ResultVO searchBookDetail(); //查看书籍数目详情
	ResultVO lendBook(String book_id,String book_status);  //借出书
	ResultVO intoBook(String book_id); //还书
	PageVO getPageForBook_history(String currentPage);  //分页获取图书记录
	PageVO getPageForBook_charge(String currentPage);  //分页获取图书收费明细
	ResultVO getPageForAll_book(String currentPage);  //分页获取全部图书
	ResultVO searchBook(String book_name);  //根据书名模糊查询
	ResultVO searchBookForLike(String book_name) ;  //模糊查询书本详情
}
