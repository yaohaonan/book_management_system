package service.serviceImpl;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import dao.BookDao;
import dao.Book_chargeDao;
import dao.Book_historyDao;
import dao.Book_klassDao;
import dao.Book_locationDao;
import dto.BookDTO;
import dto.BookDetailDTO;
import entity.Book;
import entity.Book_klass;
import entity.Book_location;
import enums.Book_status_Enum;
import enums.ResultEnum;
import service.BookService;
import util.ResultVOUtil;
import vo.PageVO;
import vo.ResultVO;

public class BookServiceImpl implements BookService{
	private Book_klassDao book_klassDao = new Book_klassDao();
	private Book_locationDao book_locationDao = new Book_locationDao();
	private BookDao bookDao = new BookDao();
	private Book_historyDao book_hisdao = new Book_historyDao();
	private Book_chargeDao book_chargedao = new Book_chargeDao();
	@Override
	public List<Book_klass> getBook_klass() {
		return	book_klassDao.findAllForBook_klass();
	}
	@Override
	public  ResultVO saveBook(String bookName, String klassName,String fileName) {  //保存书名
		if(bookName==null|klassName==null) {
			return ResultVOUtil.error(ResultEnum.PARAMETER_NULL.getCode(), ResultEnum.PARAMETER_NULL.getMessage());
		}
		ResultVO result = getFreeLocation(klassName);
		if(result.getCode()!=0) {  
			return result;   //直接抛出去
		}
		Book_klass book_klass = book_klassDao.findByName(klassName);
		Book book = bookDao.save(bookName, result.getData().toString(), book_klass.getId(),fileName);
		if(book==null) {
			return ResultVOUtil.error(ResultEnum.SAVE_BOOK_FAIL.getCode(), ResultEnum.SAVE_BOOK_FAIL.getMessage());
		}
		BookDTO bookDTO = new BookDTO();
		bookDTO.setBook_name(book.getBook_name()); 
		bookDTO.setBook_location(book.getBook_location_id());
		bookDTO.setBook_status(book.getBook_status());
		bookDTO.setId(book.getId());
		bookDTO.setKlassName(klassName);
		return ResultVOUtil.success(bookDTO);
	}
	@Override
	public synchronized ResultVO getFreeLocation(String klassName) {  //根据图书类得到空余图书位
		if(klassName==null) {
			return ResultVOUtil.error(ResultEnum.PARAMETER_NULL.getCode(), ResultEnum.PARAMETER_NULL.getMessage());
		}
		Book_klass book_klass = book_klassDao.findByName(klassName);
		if(book_klass == null) {
			return ResultVOUtil.error(ResultEnum.KLASS_NOTFOUT.getCode(), ResultEnum.KLASS_NOTFOUT.getMessage());
		}
		System.out.println("得到书类："+book_klass.toString());
		List<String> list = book_locationDao.getFreeLocation(book_klass.getId());	
		if(list==null) {
			return ResultVOUtil.error(ResultEnum.LOCATION_NOTFOUNT.getCode(), ResultEnum.LOCATION_NOTFOUNT.getMessage());
		}else {
			if(list.size()==0) {
				return ResultVOUtil.error(ResultEnum.LOCATION_NOTFOUNT.getCode(), ResultEnum.LOCATION_NOTFOUNT.getMessage());
			}else {
				Random random = new Random();
				String randomString = list.get(random.nextInt(list.size()));   //随机分配的图书位
				System.out.println("空余位置： "+randomString);
				return ResultVOUtil.success(randomString);
			}
		}
	}
	public PageVO getPageForBook_Location(String klass_id,String currentPage) {
		return	book_locationDao.getKlassForLocation(klass_id, currentPage);
	}
	@Override
	public Book_klass findByName(String name) {
		return book_klassDao.findByName(name);
	}
	@Override
	public ResultVO deleteLocation(String location_id) {
		if(location_id==null) {
			return ResultVOUtil.error(ResultEnum.PARAMETER_NULL.getCode(), ResultEnum.PARAMETER_NULL.getMessage());
		}
		ResultVO resultVO = book_locationDao.deleteBook_location(location_id);
		if(resultVO!=null) {
			return resultVO;
		}
		else {
			return ResultVOUtil.error(ResultEnum.UNKNOW_ERROR.getCode(), ResultEnum.UNKNOW_ERROR.getMessage()) ;
		}
	}
	@Override
	public ResultVO getNumberForLocation(String klassName) {
		if(klassName==null) {
			return ResultVOUtil.error(ResultEnum.PARAMETER_NULL.getCode(), ResultEnum.PARAMETER_NULL.getMessage());
		}
		String klass_id = book_klassDao.findByName(klassName).getId();
		ResultVO resultVO = book_locationDao.getNumberForLocation(klass_id);
		if(resultVO!=null) {
			return resultVO;
		}
		else {
			return ResultVOUtil.error(ResultEnum.UNKNOW_ERROR.getCode(), ResultEnum.UNKNOW_ERROR.getMessage()) ;
		}
	}
	@Override
	public ResultVO addBook_location(String klassName, String number) {
		 Pattern pattern = Pattern.compile("[0-9]*"); 
		if(pattern.matcher(number).matches()) {
			Book_klass book_klass =	book_klassDao.findByName(klassName);
			if(book_klass!=null) {
				return 	book_locationDao.addBook_location(book_klass.getId(), number);
			}else {
				return ResultVOUtil.error(ResultEnum.KLASS_NOTFOUT.getCode(), ResultEnum.KLASS_NOTFOUT.getMessage());
			}
		}else {
			return ResultVOUtil.error(ResultEnum.PARAMETER_ERROR.getCode(), ResultEnum.PARAMETER_ERROR.getMessage());
		}
	}
	@Override
	public ResultVO alterKlass_id(String klassName, String location_id) {
		Book_location book_location = book_locationDao.getBook_location(location_id);//得到该图书位
		Book_klass book_klass = book_klassDao.findByName(klassName);
		if(book_klass==null) {
			return ResultVOUtil.error(ResultEnum.KLASS_NOTFOUT.getCode(), ResultEnum.KLASS_NOTFOUT.getMessage());
		}
		if(book_location.getBook_klass_id().equals(book_klass.getId())) {  //重复修改
			return ResultVOUtil.error(ResultEnum.PARAMETER_REPEAT.getCode(), ResultEnum.PARAMETER_REPEAT.getMessage());		
		}else {
			book_locationDao.alterKlass_id(book_klass.getId(), location_id);
			return ResultVOUtil.success();
		}
	}
	@Override
	public ResultVO getPageForBook(String book_status, String currentPage) {		
		PageVO pageVO  = bookDao.getBookForStauts(book_status, currentPage);
		if(pageVO==null) {
			return ResultVOUtil.error(ResultEnum.BOOK_GET_ERROR.getCode(), ResultEnum.BOOK_GET_ERROR.getMessage());
		}else {
			return ResultVOUtil.success(pageVO);
		}
	}
	@Override
	public ResultVO deleteBook(String book_id) {
		if(book_id==null) {
			return ResultVOUtil.error(ResultEnum.PARAMETER_ERROR.getCode(), ResultEnum.PARAMETER_ERROR.getMessage());
		}else {
			return	bookDao.deleteBook(book_id);
		}
	}
	@Override
	public ResultVO searchBookDetail() {
		BookDetailDTO bookDetailDTO = bookDao.searchBookDetail();
		if(bookDetailDTO==null) {
			return ResultVOUtil.error(ResultEnum.BOOK_DETAIL_ERROR.getCode(), ResultEnum.BOOK_DETAIL_ERROR.getMessage());
		}
		return ResultVOUtil.success(bookDao.searchBookDetail());
	}
	@Override
	public ResultVO lendBook(String book_id, String book_status) {
		ResultVO resultVO;
		if(Book_status_Enum.BOOK_IN.getCode().equals(book_status)) {
			 resultVO = 	bookDao.LendBook(book_id, Book_status_Enum.BOOK_OUT.getCode());
		}else if(Book_status_Enum.BOOK_OUT.getCode().equals(book_status)) {
			 resultVO = 	bookDao.LendBook(book_id, Book_status_Enum.BOOK_IN.getCode());
		}else {
			resultVO = ResultVOUtil.error(ResultEnum.BOOK_HISTORY_SAVE_ERROR.getCode(), ResultEnum.BOOK_HISTORY_SAVE_ERROR.getMessage());
		}
		return resultVO;
	}
	@Override
	public ResultVO intoBook(String book_id) {
		String klassName = book_klassDao.findByBook_id(book_id);
		Object location_id = getFreeLocation(klassName).getData();
		if(location_id==null) {
			return ResultVOUtil.error(ResultEnum.LOCATION_NOTFOUNT.getCode(), ResultEnum.LOCATION_NOTFOUNT.getMessage());
		}
		return	bookDao.intoBook(book_id, Book_status_Enum.BOOK_IN.getCode(), location_id.toString(), klassName);
	}
	@Override
	public PageVO getPageForBook_history(String currentPage) {
		return book_hisdao.getBookHistory(currentPage);
	}
	@Override
	public PageVO getPageForBook_charge(String currentPage) {
		return book_chargedao.getBookHistory(currentPage);
	}
	@Override
	public ResultVO getPageForAll_book(String currentPage) {
		return bookDao.getPageForAll_book(currentPage);
	}
	@Override
	public ResultVO searchBook(String book_name) {
		return bookDao.searchBookName(book_name);
	}
	@Override
	public ResultVO searchBookForLike(String book_name) {
		return bookDao.searchBookForLike(book_name);
	}
}
