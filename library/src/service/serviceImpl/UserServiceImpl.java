package service.serviceImpl;

import dao.UserDao;
import entity.User_;
import service.UserService;

public class UserServiceImpl  implements UserService{
	private UserDao userDao = new UserDao();
	@Override
	public User_ login(String username, String password) {
		return userDao.findByUsernameAndPassword(username, password);
	}
}
