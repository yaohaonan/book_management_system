package test;

import com.alibaba.fastjson.JSON;

import dao.BookDao;
import vo.ResultVO;

public class TestDemo {
		public static void main(String[] args) {
			
				BookDao bookDao = new BookDao();
				ResultVO resultVO = bookDao.searchBookName("2");
				String str =	JSON.toJSONString(resultVO);
				System.out.println(str);
		}
}
