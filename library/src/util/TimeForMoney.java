package util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeForMoney {
	/*
	 * 5.计价按阶梯计算
		  A类图书
		  8:00-22:00 0.2元/h
		  22:00-8:00 0.5元/晚
		  单日封顶3元 过夜费计入前一日费用（纳入封顶计算)		  
		  B类图书
		  8:00-22:00 0.3元/h
		  22:00-8:00 0.7元/晚
		  单日封顶4元 过夜费计入前一日费用（纳入封顶计算)
		
		  不足一小时 按一小时计 
	 * */
	public static Double getAllmoney2(Long endTime,Long lend_time,String klassName)throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date endDate = new Date(endTime);
		if(endDate.getMinutes()>0|endDate.getSeconds()>0) {   //加上后面的时间是否需要补上一小时
			long	 temp_endTime  = endTime +3600000;
			String formatTimeStr = sdf.format(new Date(temp_endTime));
			String [] arrayTime = formatTimeStr.split(":");
			String endTimeStr = arrayTime[0]+":00"+":00";
			endDate = sdf.parse(endTimeStr);
			endTime = endDate.getTime();
		}
		long hours = (endTime)-new Long(lend_time);
		long all_hours = hours%3600000>0?hours/3600000+1:hours/3600000;
	
		SimpleDateFormat simple  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = simple.parse(simple.format((new Long(lend_time)))); //得到Date格式的最初时间
		Date date2 = simple.parse(simple.format((endTime))); //得到Date格式的最后时间
		System.out.println("最初开始时间："+simple.format((new Long(lend_time))));
		System.out.println("最后结束时间："+simple.format((new Long(endTime))));
		int  startHours  = date.getHours();  //得到最初的小时-->开始
		int  endHours = date2.getHours();
		System.out.println("一共花了小时数："+all_hours);
		System.out.println("开始是几点："+startHours);
		BigDecimal allMoney = new BigDecimal("0.0");  //初始算计总金额
		BigDecimal sealRoof = new BigDecimal("0.0");  //一天的封顶价
		if("A".equals(klassName)) {
			System.out.println("A类价格开始计算");
			for(int i =0;i<new Long(all_hours).intValue();i++) {
				System.out.println("当前"+startHours+"点");
				if(startHours>=8&&startHours<=22) {  //8:00-22:00 0.2元/h
						if(startHours==8) {  //重置封顶价格
							sealRoof = new BigDecimal("0.0");
							System.out.println("封顶重置");
						}
						if(sealRoof.add(new BigDecimal("0.2")).doubleValue()<3.0) {  //没超过封顶价格才加上去
							allMoney = allMoney.add(new BigDecimal("0.2")); //总金额增加	
							sealRoof = sealRoof.add(new BigDecimal("0.2"));  //封顶值增加
							System.out.println("日常过一小时已增加到："+allMoney.doubleValue());
							System.out.println("当前封顶值为："+sealRoof.doubleValue());
						}else {
							System.out.println("日常已到达封顶");
							allMoney = allMoney.add(new BigDecimal("3.0").subtract(sealRoof)); //总金额增加	
							sealRoof = sealRoof.add(new BigDecimal("3.0").subtract(sealRoof));  //封顶值增加
						}
						startHours++;   //按小时计
				}else {      //22:00-8:00 0.5元/晚(如果是这个时间内直接算过夜)
					if(sealRoof.add(new BigDecimal("0.5")).doubleValue()<3.0) {  //没超过封顶价格才加上去
								allMoney = allMoney.add(new BigDecimal("0.5"));
								System.out.println("过夜费已增加，总金额已到达："+allMoney.doubleValue());			
					}else {
						allMoney = allMoney.add(new BigDecimal("3.0").subtract(sealRoof)); 
						System.out.println("已到达当天封顶费用："+allMoney.doubleValue());
					}
					if(startHours>22) {   //时间过夜(半夜借书考虑)
						int night = new BigDecimal("32").subtract(new BigDecimal(startHours)).intValue(); 
						//直接跳到第二天。并减去消耗小时数
						System.out.println("22点过后，24点前---历经夜晚小时时间："+night);
						 i = i +night-1; //for循环自增+1 抵消
						startHours = 8; 
					}else {   //12点过后，8点前的时间
						int night = new BigDecimal("8").subtract(new BigDecimal(startHours)).intValue(); 
						//直接跳到第二天。并减去消耗小时数
						System.out.println("12点过后，8点前的时间---历经夜晚小时时间："+night);
						 i = i +night-1; //for循环自增+1 抵消
						startHours = 8; 
					}
					
				}											
		}
		}else if("B".equals(klassName)) {
			System.out.println("B类价格开始计算");
			for(int i =0;i<new Long(all_hours).intValue();i++) {
				System.out.println("当前"+startHours+"点");
				if(startHours>=8&&startHours<=22) {  //8:00-22:00 0.3元/h
						if(startHours==8) {  //重置封顶价格
							sealRoof = new BigDecimal("0.0");
							System.out.println("封顶重置");
						}
						if(sealRoof.add(new BigDecimal("0.3")).doubleValue()<=4.0) {  //没超过封顶价格才加上去
							allMoney = allMoney.add(new BigDecimal("0.3")); //总金额增加	
							sealRoof = sealRoof.add(new BigDecimal("0.3"));  //封顶值增加
							System.out.println("日常过一小时已增加到："+allMoney.doubleValue());
							System.out.println("当前封顶值为："+sealRoof.doubleValue());
						}else {
							System.out.println("日常已到达封顶");
							allMoney = allMoney.add(new BigDecimal("4.0").subtract(sealRoof)); //总金额增加	
							sealRoof = sealRoof.add(new BigDecimal("4.0").subtract(sealRoof));  //封顶值增加
						}
						startHours++;   //按小时计
				}else {      //22:00-8:00 0.7元/晚(如果是这个时间内直接算过夜)
					if(sealRoof.doubleValue()<=4.0) {  //没超过封顶价格才加上去
							if(sealRoof.add(new BigDecimal("0.7")).doubleValue()<4.0) {  //加过夜费不超过封顶
								allMoney = allMoney.add(new BigDecimal("0.7"));
								System.out.println("过夜费已增加，总金额已到达："+allMoney.doubleValue());
							}else {   //加上过夜费超过封顶
								allMoney = allMoney.add(new BigDecimal("4.0").subtract(sealRoof)); //直接等于封顶费用
								System.out.println("已到达当天封顶费用："+allMoney.doubleValue());
							}
					} 
					if(startHours>=22) {   //时间过夜(半夜借书考虑)
						int night = new BigDecimal("32").subtract(new BigDecimal(startHours)).intValue(); 
						//直接跳到第二天。并减去消耗小时数
						System.out.println("22点过后，24点前---历经夜晚小时时间："+night);
						 i = i +night-1; //for循环自增+1 抵消
						startHours = 8; 
					}else {   //12点过后，8点前的时间
						int night = new BigDecimal("8").subtract(new BigDecimal(startHours)).intValue(); 
						//直接跳到第二天。并减去消耗小时数
						System.out.println("12点过后，8点前的时间---历经夜晚小时时间："+night);
						 i = i +night-1; //for循环自增+1 抵消
						startHours = 8; 
					}
				}											
		}			
	}else {
		//未知类
	}
	System.out.println("总金额为："+allMoney);
	return allMoney.doubleValue();	
	}
	
	public static void main(String[] args) throws Exception{
		String lend_TimeStr = "2018-05-28 08:01:00";  //借出去的时间
		String endTimeStr = "2018-05-28 09:03:10";   //还书的时间
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date  lend_Time = sdf.parse(lend_TimeStr);	
		Long lend_Time_stamp = lend_Time.getTime();
		Date  endTime = sdf.parse(endTimeStr);
		getAllmoney2(endTime.getTime(),lend_Time_stamp,"B");
	}
}
