package vo;

public class LocationVO {
	private int remain;  //剩余个数
	private int occupy; //占用个数
	private int total;    //全部个数
	public int getRemain() {
		return remain;
	}
	public void setRemain(int remain) {
		this.remain = remain;
	}
	public int getOccupy() {
		return occupy;
	}
	public void setOccupy(int occupy) {
		this.occupy = occupy;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}
