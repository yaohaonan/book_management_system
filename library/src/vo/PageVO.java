package vo;

import java.util.List;

public class PageVO<T> {
		private int  totalPage;  //总页数
		private String currentPage;  //当前页
		private List<T> listData;   //数据
		private int  number;  //共多少条
		public int getTotalPage() {
			return totalPage;
		}
		public void setTotalPage(int totalPage) {
			this.totalPage = totalPage;
		}
		public String getCurrentPage() {
			return currentPage;
		}
		public void setCurrentPage(String currentPage) {
			this.currentPage = currentPage;
		}
		public List<T> getListData() {
			return listData;
		}
		public void setListData(List<T> listData) {
			this.listData = listData;
		}
		public int getNumber() {
			return number;
		}
		public void setNumber(int number) {
			this.number = number;
		}
		
		
}
